# typernode

an interface to Bitcoin-Core's `bitcoin-cli` with `HWI` based support for coldcard and other handy bitcoin tools. 
Written in typescript.

cyphernode's typed cousin.

## dev environment

### format to run unit tests

```
cd /path/to/module
mocha -r ts-node/register module_name.spec.ts --exit --timeout 12000 --timeout 5000
```

timeout (ms) is used if the process being tested takes more than default 2000 ms, usually used for tests that call external apis

This is what a timeout error looks like and can be resolved by adding a higher timeout value:

```
Error: Timeout of 3600ms exceeded. For async tests and hooks, ensure "done()" is called; if returning a Promise, ensure it resolves. (/home/ishi/api.sats.cc/typescript/src/services/core/auth/totp/speakeasy.spec.ts)
      at listOnTimeout (internal/timers.js:549:17)
```

# test all independent lib modules

```
cd $HOME/api.sats.cc/typescript/src/lib/crypto
mocha -r ts-node/register crypto.spec.ts --exit --timeout 12000

cd ../errors
mocha -r ts-node/register errors.spec.ts --exit --timeout 12000

cd ../logger
mocha -r ts-node/register logger.spec.ts --exit --timeout 12000

cd ../time
mocha -r ts-node/register time.spec.ts --exit --timeout 12000

cd ../uid
mocha -r ts-node/register uid.spec.ts --exit --timeout 12000

```

Testing kms requires an access token. 

Request admin for access token and test kms


```
export VAULT_TOKEN=s.T0K3NProV1d3D8yA0M1N
source $HOME/.bashrc

cd ../kms
mocha -r ts-node/register kms.spec.ts --exit --timeout 12000 --timeout 6500

```


#### All successive tests require infrastructural dependencies

## infrastructure/mongo
## infrastructure/bitcoin
## infrastructure/vault


### vishalmenon.92@gmail.com
