/*
api.sats.cc: mongo profile specifications
Matrix Network International B.V.
developed by (•̪●)==ε0O o o oo o o o o O1shi`(•.°)~#
*/

// IMPORTS
import { expect } from "chai";
import "mocha";
import * as  mongo from "./mongo";
import {DbConnection} from  "./interface";


// ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------
// GLOBAL CONFIGURATIONS
const db = new mongo.MongoDatabase();
const connection: DbConnection = {
 port: process.env.DB_PORT,
 ip : process.env.DB_IP,
 name: 'sats',
 auth: 'auth_server:supersecret',
}
// ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------
describe("Initalizing Test: Mongo Connection ", function () {

 describe("mongo.connect()", function () {
  it("SHOULD open a connection to mongodb", async function () {
   const database = await db.connect(connection);
   expect(database).to.be.a('object')
  });
 });


});

// ------------------ '(◣ ◢)' ---------------------
