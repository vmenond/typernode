export interface KMS{
 unlockAppSecret(key: string):Promise<string |Error>;
 allowService(service: ProtectedServices, password: string);
 getTFAKey():Promise<string | Error>;
 getProfileKey(): Promise<string | Error>;
 getBitcoinRPCAuth():Promise<string | Error>;
}
// ------------------ '(◣ ◢)' ---------------------
export enum Environment{
  D = "DEV",
  P = "PROD"
}
export enum ProtectedServices {
 "SPC" = "shufti_check",
 "BCU" = "bitcoin_update",
 "BL"  = "balances",
 "ADP" = "admin_pass",
 "DP"  = "delete_profile",
 "DTF" = "delete_tfa"
} 



// -------------------------------------------------