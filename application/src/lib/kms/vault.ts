const request =require("request-promise");
import {
  KMS, 
  ProtectedServices,
}  from "./interface";
import { logger } from "../logger/winston";
import { handleError } from "../errors/e";
import { S5Crypto } from "../crypto/crypto";
const env = process.env.NODE_ENV;
const secret_root = ((env==="PROD")?"sats-cc":"sats-dev");
const vault_location = process.env.VAULT_ADDR;
const s5crypto = new S5Crypto();

// ------------------ '(◣ ◢)' -----------------------

export class S5Vault implements KMS{
  async unlockAppSecret(key: string): Promise<string | Error> {
    try {
      console.log({env,vault_location})
      const secrets_crypted = (env==="PROD")?crypted_prod:crypted_dev;
      
      const g = await getGSec();
      if(g instanceof Error) return g;
     
      return await s5crypto.decryptAESMessageWithIV(secrets_crypted[key], g);
  
    } catch (e) {
      logger.error(e);
      return (handleError({
        "App secret unlock Error": e
      }));
    }
  }
  async allowService(service, password){
    switch (service) {
      case "shufti_check":
        const sc = await unlock("SHUFTI_SEND_SECRET");
        // logger.debug({ sc });
        if (sc instanceof Error) return sc;
        if (password === sc) {
          return true;
        } else {
          return handleError({
            code: 401,
            message: "Wrong"
          });
        }
        break;
  
      case "bitcoin_update":
        const cu = await unlock("CORE_NOTIFY_SECRET");
        if (cu instanceof Error) return cu;
        if (password === cu) {
          return true;
        } else {
          return handleError({
            code: 401,
            message: "Wrong"
          });
        }
        break;
  
      case "balances":
        const bs = await unlock("BALANCES_SECRET");
        if (bs instanceof Error) return bs;
        if (password === bs) {
          return true;
        } else {
          return handleError({
            code: 401,
            message: "Wrong"
          });
        }
        break;
  
      case "admin_pass":
        const al = await unlock("ADMIN_PASS");
        console.log(al)
        if (al instanceof Error) return al;
        if (password === al) {
          return true;
        } else {
          return handleError({
            code: 401,
            message: "Wrong"
          });
        }
        break;
  
      case "delete_profile":
        const dp = await unlock("PROFILE_DEL_SECRET");
        if (dp instanceof Error) return dp;
        if (password === dp) {
          return true;
        } else {
          return handleError({
            code: 401,
            message: "Wrong"
          });
        }
        break;
  
      case "delete_tfa":
        const dt = await unlock("TFA_DEL_SECRET");
        if (dt instanceof Error) return dt;
        if (password === dt) {
          return true;
        } else {
          return handleError({
            code: 401,
            message: "Wrong"
          });
        }
        break;
  
      default:
        return handleError({
          code: 404,
          message: "Wrong Service Name"
        });
    }
  }
  async getTFAKey(){
    try {
      const options = {
        url: `${vault_location}/${secret_root}/moltres/mongo/tfa_crypt`,
        method: "GET",
        headers: {
          "X-Vault-Token": process.env.VAULT_TOKEN
        }
      };
  
      let response = await request(options);
      response = JSON.parse(response);
      return response.data.secret;
    } catch (e) {
      return handleError({
        code: 500,
        message: e.message
      });
    }
  }
  async getProfileKey(){
    try {
      const options = {
        url: `${vault_location}/${secret_root}/moltres/mongo/profile_crypt`,
        method: "GET",
        headers: {
          "X-Vault-Token": process.env.VAULT_TOKEN
        }
      };
  
      let response = await request(options);
      response = JSON.parse(response);
      return response.data.secret;
    } catch (e) {
      return handleError({
        "Vault Error": e.message
      });
    }
  }
  async getMongoDbAuth():Promise<string | Error>{
    try {
      const options = {
        url: `${vault_location}/${secret_root}/moltres/mongo/pass`,
        method: "GET",
        headers: {
          "X-Vault-Token": process.env.VAULT_TOKEN
        }
      };
  
      let response = await request(options);
      response = JSON.parse(response);
      return response.data.secret;
    } catch (e) {
      return handleError({
        code: 500,
        message: e.message
      });
    }
  }
  async getBitcoinRPCAuth():Promise<string | Error>{
    try {
      const options = {
        url: `${vault_location}/${secret_root}/moltres/bitcoind/rpc`,
        method: "GET",
        headers: {
          "X-Vault-Token": process.env.VAULT_TOKEN
        }
      };
  
      let response = await request(options);
      response = JSON.parse(response);
      return response.data.secret;
    } catch (e) {
      return handleError({
        code: 500,
        message: e.message
      });
    }
  }
  async getBitcoinWalletAuth(wallet_name: string):Promise<string | Error>{
    try {
      const options = {
        url: `${vault_location}/${secret_root}/moltres/bitcoind/wallet/${(wallet_name==="")?"root":wallet_name}`,
        method: "GET",
        headers: {
          "X-Vault-Token": process.env.VAULT_TOKEN
        }
      };
      let response = await request(options);
      response = JSON.parse(response);
      return response.data.secret;
    } catch (e) {
      return handleError({
        code: 500,
        message: e.message
      });
    }
  }

  async getCCPSBTPass():Promise<string | Error>{
    try {
      const options = {
        url: `${vault_location}/${secret_root}/moltres/bitcoind/wallet/cc_pass`,
        method: "GET",
        headers: {
          "X-Vault-Token": process.env.VAULT_TOKEN
        }
      };
  
      let response = await request(options);
      response = JSON.parse(response);
      return response.data.secret;
    } catch (e) {
      return handleError({
        code: 500,
        message: e.message
      });
    }
  }
  async getCrystalApiKey():Promise<string | Error>{
    try {
      const options = {
        url: `${vault_location}/${secret_root}/crystal/api`,
        method: "GET",
        headers: {
          "X-Vault-Token": process.env.VAULT_TOKEN
        }
      };
  
      let response = await request(options);
      response = JSON.parse(response);
      return response.data.secret;
    } catch (e) {
      return handleError({
        code: 500,
        message: e.message
      });
    }
  }
  async getFirebasePrivKey():Promise<string | Error>{
    try {
      const options = {
        url: `${vault_location}/${secret_root}/moltres/node/firebase/key`,
        method: "GET",
        headers: {
          "X-Vault-Token": process.env.VAULT_TOKEN
        }
      };
  
      let response = await request(options);
      response = JSON.parse(response);
      return response.data.secret;
    } catch (e) {
      return handleError({
        code: 500,
        message: e.message
      });
    }
  }
  
}
// ------------------ '(◣ ◢)' -----------------------

export async function getGSec(): Promise<string | Error> {
 
    try {
      const options = {
        url: `${vault_location}/${secret_root}/moltres/node/gsec`,
        method: "GET",
        headers: {
          "X-Vault-Token": process.env.VAULT_TOKEN
        }
      };

      let response = await request.get(options);
      response = JSON.parse(response);
      return (response.data.secret);
    } catch (e) {
      return(handleError({
        "Vault Error": e.message
      }));
    }
}
export async function unlock(key: string): Promise<string | Error> {
  try {
    const secrets_crypted = (env==="PROD")?crypted_prod:crypted_dev;
    
    const g = await getGSec();
    if(g instanceof Error) return g;
   
    return await s5crypto.decryptAESMessageWithIV(secrets_crypted[key], g);

  } catch (e) {
    logger.error(e);
    return (handleError({
      "App secret unlock Error": e
    }));
  }
}

// ---------------I||:CRYPTED:||I----------------------
const crypted_dev = {
  DB_AUTH: '30d4f1ed58bfbc623eae08c914355df4:JB1a2OIi6w2XkPicODpF9w==',
  KEY_PATH: '88a0d587df3f0905610e2cae9e41d485:ljjl7nOYqQQXgQ14jAiaHVJRNfWegY515BPr9I9Zzeo=',
  SATS_PRIV: '90c6db1748dcbc60f4385f4790ddf2bd:zDaHzk0jJx4tIa8NdtDexw==',
  CORE_NOTIFY_SECRET: 'f605cc9a078bc9caf61246d80dfa7b1d:Z7ZZv+avGjJc43w3knGe0w==',
  SLACK_UPDATE_URI: '24c4edd350fdb57c74086395ee5a1c29:pAKtr79sKcZjZYIk1z8AVg==',
  SLACK_TOKEN: 'fa139a82998b67a1d94671f21cb24496:xrEAytLVMEppZxS7qb54Pw==',
  SLACK_REPORT_URI: 'b87868d90d724a4ce14c6be5472ce09d:jckBaSqVwNnlJvu4SK2v7g==',
  BALANCES_SECRET: '34fd69de8a654ed6042e60724551ff2b:oQ3uJsyCGcxbwZPO4Nqnnw==',
  ADMIN_PASS: 'e00e34145a745e3a56c023b1ed48f5bd:W2fYt1q5tg4TaP9fLEknw0npr27KjNppB47H8E9vkBs=',
  SATS_MFA_ACCESS_TOKEN_SECRET: '68e2c647996c4ea04ac65a00ed94563e:alSOrWP0uUIgg5Yax7IekQ=='

};
// ------------------ '(◣ ◢)' -----------------------
const crypted_prod = {
  DB_AUTH: "ab8026cd4baca54795b4f15fe0e4611c:FTMah8ZPgV8+gDdOObQW8w==",
  MEW_URI:
    "8708e8ec29405d2a8d18ed1811f4b436:blNr0uxE+AUw9bdKIlIezWQFO2/kMEOlqzdyw5Je4H4=",
  PROFILE_DEL_SECRET:
    "09968c2c770424013287c4b7e217f72a:lmOhLQmdAaFSaxWkdD3ckFNBTAWHtu1YUs4g/DHZC5GY+es9mNe0oMtPsyjO4bd4",
  CREATE_HMAC_SECRET:
    "42bc27d2ea57aa2337d6179fe75f8ee6:xbgoKep+OHaYwXWb9FrB1Pqu1VAYIJmFS3A9FsxdU0T0do+3oFLXfjgRMebyzQIPGLLe3XAdOvGDuIioTvheIw==",
  SATS_PRIV:
    "14a932b554c3c4e7ce19a19c79176356:LTaschNNeicd+sBGe1Hklhz0muz9wRx1xEVTTZjtNh5II41T653mXD/HRvkBrOA2csUduFq8nxmWspSyRZmNEpmFUAQ2r9Wmmf9TX33Z0WU=",
  CORE_NOTIFY_SECRET:
    "25c54dbb92565013b74b31d983e684b9:htqtle7AzA6egMH19X65/489HOxY4gL2H0z/Bv2P2Tc=",
  LOG_SECRET: "fec3d5f7853d9cc4847e5b4588bc9161:yeuymaslfPleL1ksjqjuzg==",
  SHUFTI_ID:
    "7d160936f4eea07e5cc37396cbc24b7b:3dwkmnIk3L/iRYLfaxc2vdJ/UojHaneiJ8ZCIb7cK8TwApIHvswzNqr+QmbWSL8bJKnfn4fd3IBP+n6uyrTd1A==",
  SLACK_UPDATE_URI:
  "b647ce887d99d43081549e738dad1530:vctW7o+nH1VCCXfaJYGTxIaH1+Y/Q4mZQ2Tv7QZ+hqMt+Nj2KB71TbGCFPSSNLHjkHReBxGnBPwY8Ib5YUQ6Jp1GrUBurtMumA/16kdGfQo=",
  SLACK_TOKEN:
  "edb149948cb44d757198c3b3232d4766:/SyAgoUMIt9HhTnQ8UPWp0Vyq5b3KHdV73AlJi78tR7JvRUdYxT9Jm6yWMJgJvqOTprLxFKdO3eAL0UXlBk5te9BL7gdtjTvG5UXFit3Q/o=",
  SLACK_REPORT_URI:
    "7f706ea343530ba8780dc90f7ea9849d:23WaTFwCuyOqP9GJG4nnNvvX71BDpUrTfZiilEU1yNcTUxZG64K9ENFKAlMf+SHAvwLgIwd0pVIAUNJ2eovM9QjwGdm9JgrXslIVq36TI3Y=",
  BALANCES_SECRET: "74f81230d1b62f6bcde2356e3b46d404:wLLHoe2MiMI75IXz6fBshg==",
  ADMIN_PASS: "8a099cb622bba8024cf75ba9400ab9f7:qCQcCCYU/w86aJNEUWtVHT6IQCcotuX1YQmTykxsPgk=",
  TFA_DEL_SECRET:
    "79e942ae9d6099a93309d6a4b2e3e955:73Z2q9Cy1G9dXL8mOF5zw/jMaOLnhtyX6ahiih8L9+w=",
  SATS_MFA_ACCESS_TOKEN_SECRET:
    "61c0990d6c4ea080ee3f722fc2478135:pkTLIf6B7Zrh+p+0wsEcpg=="
};
// ---------------I||:CRYPTED:||I----------------------
