/*
api.sats.cc: lib specifications
Matrix Network International B.V.
developed by (•̪●)==ε0O o o oo o o o o O1shi`(•.°)~#
*/

import { expect } from "chai";
import "mocha";
import {
    ProtectedServices
} from "./interface";
import * as vault from "./vault";

const kms = new vault.S5Vault();
// ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------
// GLOBAL CONFIGURATIONS
const keys = [
    "DB_AUTH",
    "DB_USER",
    "BALANCES",

]
const values = [
    "sats",
    "supersecret",
    "23098098iodjke",
]
// ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------
describe("Initalizing Test: crypted ", function () {
    describe.skip("unlockAppSecret", function () {
        it("SHOULD unlock a key from the crypted", async function () {
            const decrypted = await kms.unlockAppSecret(keys[0]);
            expect(decrypted).to.equal(values[1]);
        });
    });
    describe.skip("allowService", function () {
        it("SHOULD check a password against a service from the crypted", async function () {
            const status = await kms.allowService(ProtectedServices.BL, values[2]);
            expect(status).to.equal(true);
        });
    });
    describe("getMongoDbAuth", function () {
        it("SHOULD getMongoDbAuth", async function () {
            const status = await kms.getMongoDbAuth();
            expect(status).to.equal(values[1]);
        });
    });

});

// ------------------ '(◣ ◢)' ---------------------
