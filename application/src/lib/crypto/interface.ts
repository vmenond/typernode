export interface CryptoInterface {
 signS256Message(message: string, secret: string): string | Error;
 checkS256Signature(message: string, sig: string, secret: string): boolean | Error;
 createRSAPairFile(filename: string): Promise<string | Error>; 
 getECDHPair(): ECDHPair | Error;
 encryptAESMessageWithIV(text: string, key_hex: string): string  | Error;
 decryptAESMessageWithIV(iv_text_crypt: string, key_hex: string): string  | Error;
 encryptFile(in_file_path: string,out_file_path: string, key: string): Promise<string | Error>;
 decryptFile(in_file_path: string,out_file_path: string, skey: string): Promise<string | Error>; 
}

export interface  ECDHPair{
 private_key: string;
 public_key: string;
}
