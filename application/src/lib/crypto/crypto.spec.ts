/*
api.sats.cc: crypto lib specifications
Matrix Network International B.V.
developed by (•̪●)==ε0O o o oo o o o o O1shi`(•.°)~#
*/

import { expect } from "chai";
import "../../services/core/bitcoin/node_modules/mocha";
import {S5Crypto} from "./crypto";
import * as fs from "fs";
import { response } from "express";
import { ECDHPair } from "./interface";
// ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------
// GLOBAL CONFIGURATIONS
const crypto = new S5Crypto();
const message = "supersecret"
const signature = "6e107bc3b820bacf23574bd2c28334f2da722fefdacafcfc6aa51a984e642299";
const rsa_filename = "crypto-test-secret";
const key_hex = "9870aa5092c44767f7a0d60cfb4dde81abc66ed8ac71bc2d466ed8b6e0323fd4";
let ecdh: ECDHPair = {
 private_key: "shhhHH!",
 public_key: "oh, hi! :)"
}
let ivcrypt_message;
const test_file_path_in = `${process.env.HOME}/api.sats.cc/typescript/src/misc/wolf.jpg`;
const test_file_path_crypt = `${process.env.HOME}/api.sats.cc/typescript/src/misc/wolf.crypt`;
const test_file_path_decrypt = test_file_path_in;
const key = "supersecret";
// ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------
describe("Initalizing Test: S5Crypto Lib ", function () {

 describe("signS256Message", function () {
  it("SHOULD return a hmac sha256 signature of a message using a secret", async function () {
   const response = crypto.signS256Message(message, key_hex);
  //  console.log({response});
   expect(response).to.equal(signature);
  });
 });
 describe("checkS256Message", function () {
  it("SHOULD check if a signture of a message is valid", async function () {
   const response = crypto.checkS256Signature(message, signature, key_hex);
   expect(response).to.equal(true);
  });
 });
 describe("createRSAPairFile", function () {
  it("SHOULD create an RSA Key Pair as a File", async function () {
   const response = await crypto.createRSAPairFile(rsa_filename);
   if(response instanceof Error) throw response;
   const status = (fs.existsSync(response+".pem") && fs.existsSync(response+".pub"))? true:false;
   fs.unlinkSync(response+".pem");
   fs.unlinkSync(response+".pub");
   expect(status).to.equal(true);
  });
 });
 describe("getECDHPair", function () {
  it("SHOULD create an ECDHPair object", async function () {
   const response = await crypto.getECDHPair();
   ecdh = response as ECDHPair;
   expect(response).to.have.property("private_key");
   expect(response).to.have.property("public_key");
  });
 });

 describe("encryptAESMessageWithIV()", function () {
  it("SHOULD encrypt a message with a 16byte/32char IV", async function () {
   const response = await crypto.encryptAESMessageWithIV(message, key_hex);
   if( response instanceof Error ) throw response;
   const iv = response.split(':')[0 ];
   ivcrypt_message = response;
  //  console.log({ivcrypt_message})
   expect(iv.length).to.equal(32);
  });
 });
 describe("decryptAESMessageWithIV()", function () {
  it("SHOULD decrypt an 16byte/32 char IV padded encrypted message", async function () {
   const response = await crypto.decryptAESMessageWithIV(ivcrypt_message, key_hex);
   expect(response).to.equal(message);
  });
 });
 describe("501: encryptAESMessageWithIV()", function () {
    it("SHOULD return 501 Error Invalid Key Length", async function () {
     const response = await crypto.encryptAESMessageWithIV(message, key);
     expect(response['name']).to.equal("501");
    });
   });
 describe("createRSAPairFile", function () {
  it("SHOULD create an RSA Key Pair as a File", async function () {
   const response = await crypto.createRSAPairFile(rsa_filename);
   if(response instanceof Error) throw response;
   const status = (fs.existsSync(response+".pem") && fs.existsSync(response+".pub"))? true:false;
   fs.unlinkSync(response+".pem");
   fs.unlinkSync(response+".pub");
   expect(status).to.equal(true);
  });
 });
 describe("encryptFile", function () {
  it("SHOULD encrypt a file at a given path", async function () {
   const response = await crypto.encryptFile(test_file_path_in,test_file_path_crypt,key);
   if(response instanceof Error) throw response;
   const status = fs.existsSync(test_file_path_crypt)? true:false;
   expect(status).to.equal(true);
  });
 });

 describe("decryptFile", function () {
  it("SHOULD decrypt a file at a given path", async function () {
   const response = await crypto.decryptFile(test_file_path_crypt,test_file_path_decrypt,key);
   if(response instanceof Error) throw response;
   const status = fs.existsSync(test_file_path_decrypt)? true:false;
   expect(status).to.equal(true);
  });
 });

});

// ------------------ '(◣ ◢)' ---------------------
