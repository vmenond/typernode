/*
api.sats.cc: auth_server lib: hmac
Matrix Network International B.V.
#
origins of the token master
21
developed by (•̪●)==ε0O o o oo o o o o O1shi`(•.°)~#
*/
// ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------
import util from "util";
import crypto from "crypto";
import fs from "fs";
// import * as gsec from "./sec";
import encryptor from "file-encryptor";
// import { S5ReponseHeaders } from "./handlers";

// import { logger } from "../aux/logger";
import { CryptoInterface, ECDHPair } from "./interface";
import { handleError } from "../errors/e";

const key_path = process.env.KEY_PATH;

export class S5Crypto implements CryptoInterface {
  signS256Message(message: string, secret: string): string | Error {
    try {
      return crypto
        .createHmac("sha256", secret)
        .update(message)
        .digest("hex");
    }
    catch (e) {
      return handleError(e);
    }
  }
  checkS256Signature(message: string, sig: string, secret: string): boolean | Error {
    try {
      const computed_signature = crypto
        .createHmac("sha256", secret)
        .update(message)
        .digest("hex");
      const status = (computed_signature === sig) ? true : false;
      return status;
    } catch (e) {
      return handleError(e);
    }
  }
  async createRSAPairFile(filename: string): Promise<string | Error> {
    try {
      const generate_key_pair = util.promisify(crypto.generateKeyPair);
      const result = await generate_key_pair("rsa", {
        modulusLength: 2048,
        publicKeyEncoding: {
          type: "spki",
          format: "pem"
        },
        privateKeyEncoding: {
          type: "pkcs8",
          format: "pem"
        }
      });
      // return { result };
      const message = "check";
      const sign = crypto.createSign("SHA256");
      sign.update(message);
      sign.end();
    
      const verify = crypto.createVerify("SHA256");
      verify.write(message);
      verify.end();
  
      fs.writeFileSync(
        `${key_path}/${filename}.pem`,
        result.privateKey.toString(),
        "utf8"
      );
      fs.writeFileSync(
        `${key_path}/${filename}.pub`,
        result.publicKey.toString(),
        "utf8"
      );
  
      return `${key_path}/${filename}`;
    } catch (e) {
      return handleError(e);
    }
  }
  getECDHPair(): ECDHPair | Error{
    try{
      const ecdh = crypto.createECDH("secp256k1");
      ecdh.generateKeys("hex");
      const private_key = ecdh.getPrivateKey("hex");
      const public_key = ecdh.getPublicKey("hex");
    
      return {
        private_key,
        public_key
      };
    }
    catch(e){
      return handleError(e);
    }
  }
  encryptAESMessageWithIV(text: string, key_hex: string): string | Error{
    try{
    const algorithm = "aes-256-cbc";
    const key = Buffer.from(key_hex, "hex");
    const IV_LENGTH = 16; // For AES, this is always 16
    const iv = crypto.randomBytes(IV_LENGTH);
    const cipher = crypto.createCipheriv("aes-256-cbc", Buffer.from(key), iv);
  
    let encrypted = cipher.update(text);
    encrypted = Buffer.concat([encrypted, cipher.final()]);
  
    const encrypted_text =
      iv.toString("hex") + ":" + encrypted.toString("base64");
  
    return encrypted_text;
    }
    catch(e){
      return handleError(e);
    }
  }
  decryptAESMessageWithIV(iv_text_crypt: string, key_hex: string): string | Error{
    try{
      const algorithm = "aes-256-cbc";
      const key = Buffer.from(key_hex, "hex");
    
      const IV_LENGTH = 16; // For AES, this is always 16
      const text_parts = iv_text_crypt.split(":");
      const iv = Buffer.from(text_parts.shift(), "hex");
      const encrypted_text = Buffer.from(text_parts.join(":"), "base64");
      const decipher = crypto.createDecipheriv("aes-256-cbc", Buffer.from(key), iv);
    
      let decrypted = decipher.update(encrypted_text);
      decrypted = Buffer.concat([decrypted, decipher.final()]);
    
      return decrypted.toString();
    }
    catch(e){
      return handleError(e);
    }
  }
  async encryptFile(in_file_path: string, out_file_path: string, key: string): Promise<string | Error>{
    try {
      if (fs.existsSync(in_file_path)) {
        const encrypt_file_promise = util.promisify(encryptor.encryptFile);
        await encrypt_file_promise(
          in_file_path,
          out_file_path,
          key,
          {
            algorithm: "aes256"
          }
        );
        fs.unlinkSync(in_file_path);
        return out_file_path;
      } else {
        return handleError("File does not exist on the system");
      }
    } catch (e) {
      return handleError(e);
    }
  }
  async decryptFile(in_file_path: string, out_file_path: string, key: string): Promise<string | Error>{
    try {
      if (fs.existsSync(in_file_path)) {
        const decrypt_file_promise = util.promisify(encryptor.decryptFile);
  
        await decrypt_file_promise(
          in_file_path,
          out_file_path,
          key,
          {
            algorithm: "aes256"
          }
        );
        fs.unlinkSync(in_file_path);
  
        return out_file_path;
      } else {
        return handleError("File does not exist on the system");
      }
    } catch (e) {
      return handleError(e);
    }
  }
}
// ------------------ '(◣ ◢)' ----------------------



// ------------------° ̿ ̿'''\̵͇̿̿\з=(◕_◕)=ε/̵͇̿̿/'̿'̿ ̿ °------------------
