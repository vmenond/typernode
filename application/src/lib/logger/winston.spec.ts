/*
api.sats.cc: logger lib specifications
Matrix Network International B.V.
developed by (•̪●)==ε0O o o oo o o o o O1shi`(•.°)~#
*/
import util from "util";

import { expect } from "chai";
import "mocha";
import {logger} from "./winston";
import * as fs from "fs";
import { exec } from 'child_process';
const LOG_FILE = `${process.env.HOME}/winston/logs/moltres.log`;

// ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------
// GLOBAL CONFIGURATIONS

// ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------
describe("Initalizing Test: logger::WINSTON ", function () {

 describe("logger.verbose()", function () {
  it("SHOULD NOT write log to file", async function () {
   logger.verbose({
    this: "******'(◣ ◢)'******"
   });
   const exec_prom = util.promisify(exec);
   const out = await exec_prom(`tail -2 ${LOG_FILE}`);
   const message = "******'(◣. .◢)'******";
   expect(out.stdout.includes(message)).to.equal(false);
  });
 });

 describe("logger.error()", function () {
  it("SHOULD write logs to file", async function () {
   logger.error({
    this: "*****'(◣ ◢)'*****"
   });
   const exec_prom = util.promisify(exec);
 const out = await exec_prom(`tail -2 ${LOG_FILE}`);
   console.log({out})
   const message = "*****'(◣ ◢)'*****";
   expect(out.stdout.includes(message)).to.equal(true);
  });
 });
});

// ------------------ '(◣ ◢)' ---------------------
