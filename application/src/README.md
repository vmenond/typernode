
# src:

## lib: common functions
  
  - crypto: sats.cc specific cryptographic operations. 
  - errors: application specific error handling
  - kms: key management service
  - logger: sats.cc custom log format
  - satsmath: fx to convert bitcoin<->sats
  - time: time helpers
  - uid: unique identifiers for internal referencing

## storage: db setup & initialization

## network: server setup & handlers

## services: application logic
  
  - aux: external
    - slack: admin notifications

  - core: local 
    - auth: to setup users & authentication
    - session: to monitor api usage activity
    - bitcoin: to delegate calls to local bitcoin node 
    - info: to provide general api info

  - run: entry points (main)

## misc: test assets & helpers

### file types: 

At the bare minimum a module has an

  - interface: a class defining all types & functions provided by a service
  - $interface: implementation of the interface, named after the service or the provider
  - $interface.spec : test

If the module requires storage, it additionally has a

  - storage: a class defining all storage capabilities of a service
  - $storage: implementation of the storage class, in this case mongo
  - $storage.spec: test

If the module exposes its interface over the network it additionally has a

  - router: declares http endpoints exposed by the service 
  - dto: handlers for router io
  - dto.spec: test 



## support: vishalmenon.92@gmail.com
