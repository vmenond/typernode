export interface AdminNotifyInterface{
 error(error: any): Promise<boolean | Error>;
 message( title: string, data: string): Promise<boolean | Error>;
 sendFile(file:S5File): Promise<boolean | Error>;
}

export interface S5File {
 path: string;
 mime?: string;
 type?: string;
 name: string;
}