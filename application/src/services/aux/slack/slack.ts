/*
api.sats.cc: auth_server lib: notify (slack)
Matrix Network International B.V.
#
21
developed by (•̪●)==ε0O o o oo o o o o O1shi`(•.°)~#
*/
// ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------

// const request = require('request-promise');
import { handleError } from "../../../lib/errors/e";
import {AdminNotifyInterface,S5File} from "./interface";

import util from "util";
import fs from "fs";
const rp = require ("request-promise");
import Slack from "slack-node";
import * as vault from "../../../lib/kms/vault";

const kms = new vault.S5Vault();

import { logger } from "../../../lib/logger/winston";

export class S5Slack implements AdminNotifyInterface{
  async error(error: string | object ): Promise<boolean | Error>{
    try {
      if (error instanceof Object) {
        error = JSON.stringify(error,null,3);
      }
      const options = {
        url: await kms.unlockAppSecret("SLACK_REPORT_URI"),
        // method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body:{text: error},
        json: true
      };
      await rp.post(options);
      return true;
    } catch (e) {
      return handleError(e);
    }
  }
  async message(title: string,data: string): Promise<boolean | Error>{
    try {
      const slack = new Slack();
      const uri = await kms.unlockAppSecret("SLACK_UPDATE_URI");
      if(uri instanceof Error) return uri;
      console.log({uri})
      slack.setWebhook(uri);
      const sendSlackMessage = util.promisify(slack.webhook);
  
      const options = {
        uri,
        channel: "#development",
        username: title,
        text: data
      };
      const response = await sendSlackMessage(options);
      if (response) {
        return true;
      } else {
        return handleError("No response from slack");
      }
    } catch (e) {
      // logger.error(e);
      return handleError(e);
    }
  }
  async sendFile(file: S5File): Promise<boolean | Error>{
    try {
      const slack_token = await kms.unlockAppSecret("SLACK_TOKEN");
      if(slack_token instanceof Error) return slack_token;
      const options = {
        url: "https://slack.com/api/files.upload",
        method: "POST",
        headers: {
          Authorization: `Bearer ${slack_token}`
        },
        formData: {
          token: slack_token,
          title: `File Verify: ${file.name}`,
          initial_comment: "Please rub the cat.",
          filename: `${file.name}_${file.type}`,
          filetype: file.mime,
          channels: "development",
          file: fs.createReadStream(file.path)
        }
      };
  
      await rp.post(options);

      return true;

    } catch (e) {
      return handleError(e);
    }
  }
}
// ------------------ '(◣ ◢)' ----------------------

// ------------------ '(◣ ◢)' ----------------------
