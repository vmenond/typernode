/*
api.sats.cc: notify lib specifications
Matrix Network International B.V.
developed by (•̪●)==ε0O o o oo o o o o O1shi`(•.°)~#
*/

import { expect } from "chai";
import "mocha";
import {S5Slack} from "./slack";

import * as sinon from "sinon";
import * as  vault from "../../../lib/kms/vault";
import * as dev_secrets from "../../../misc/dev_secrets.json";

import { S5File } from "./interface";


const alert = new S5Slack();
// ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------
// GLOBAL CONFIGURATIONS

const file: S5File =
  {
   path: `${process.env.HOME}/sats.cc/typescript/src/misc/address.jpg`,
   mime: "image/jpeg",
   type: "Government ID",
   name: "vishalmenon.92@gmail.com"
  };
// ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------
describe("Initalizing Test: Notify ", function () {
 const sandbox = sinon.createSandbox();

 before(async function () {
  const gsec_stubby = sandbox.stub(vault.S5Vault.prototype, "unlockAppSecret");
  const keys = [
   "SLACK_UPDATE_URI",
   "SLACK_TOKEN",
   "SLACK_REPORT_URI"
  ];
  keys.map(key => {
   gsec_stubby.withArgs(key).resolves(dev_secrets[key]);
  });
 });

 after(function (done) {
  sandbox.restore();
  done();
 });

 describe("error", function () {
  it("SHOULD notify admin at slack with an error", async function () {
   const response = await alert.error({code: 300, errror: {more: "ftuff"}});
   expect(response).to.equal(true);
  });
 });

 describe("update", function () {
  it("SHOULD notify admin at slack with an update", async function () {
   const response = await alert.message("testing libs at sats.cc", "update");
   expect(response).to.equal(true);
  });
 });

 describe("sendFile", function () {
  it("SHOULD notify admin at slack with a file for validation", async function () {
   const response = await alert.sendFile(file);
   console.log({response})

   expect(response).to.equal(true);
  });
 });

});

// ------------------ '(◣ ◢)' ---------------------
