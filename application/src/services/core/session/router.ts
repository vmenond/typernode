/*
api.sats.cc: moltres routes: /session
Matrix Network International B.V.
#
developed by (•̪●)==ε0O o o oo o o o o O1shi`(•.°)~
*/
// ------------------ '(◣ ◢)' ---------------------
import { Router } from "express";

import {
sessionMiddleware,
handleGetActions,
} from "./dto";

// ------------------ '(◣ ◢)' ---------------------
export const router = Router();
// ------------------ '(◣ ◢)' ---------------------
router.use(sessionMiddleware);
router.get("/actions", handleGetActions);

// ------------------° ̿ ̿'''\̵͇̿̿\з=(◕_◕)=ε/̵͇̿̿/'̿'̿ ̿ °------------------
