/*
api.sats.cc: core specifications
Matrix Network International B.V.
developed by (•̪●)==ε0O o o oo o o o o O1shi`(•.°)~#
*/

// IMPORTS
import { expect, assert } from "chai";
import "mocha";

const sinon = require("sinon");
import { MongoDatabase } from "../../../storage/mongo";
import * as controller from "./session";
import { ActionNames, SessionModel } from "./interface";
import { MFALevel } from "../auth/interface";
import { S5UID } from "../../../lib/uid/uid";
import { MongoSessionStore } from "./mongo";


const s5uid = new S5UID();

const session = new controller.S5Session();
const db = new MongoDatabase();
const store = new MongoSessionStore();

// ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------
// GLOBAL CONFIGURATIONS

let skip = 2;
let limit = 3;
const uid = "kyc0@test.com";
let action0: SessionModel = {
  uid,
  ip: "local",
  device: "Android 9",
  action: ActionNames.JWT,
  auth_level: MFALevel.B1,
  reference: s5uid.createRandomID(32),
};

let action1: SessionModel = {
  uid,
  ip: "local",
  device: "Android 9",
  action: ActionNames.BTCG,
  auth_level: MFALevel.T2,
  reference: s5uid.createRandomID(32)
};

let action2: SessionModel = {
  uid,
  ip: "local",
  device: "Android 9",
  action: ActionNames.BTCP,
  auth_level: MFALevel.B1,
  reference: s5uid.createRandomID(32),
};

// ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------
describe("Initalizing Test: Sessions ", function () {
  const sandbox = sinon.createSandbox();
  before(async function () {
    await db.connect({
      port: process.env.DB_PORT,
      ip: process.env.DB_IP,
      name: 'sats',
      auth: 'auth_server:supersecret',
    });
  });

  after(function (done) {
    sandbox.restore();
    store.deleteSession(uid);
    done();
  });

  describe("updateAction-1", function () {
    it("SHOULD updateAction-1", async function () {
      const response = await session.updateAction(action0);
      expect(response).to.equal(true);
    });
  });
  describe("updateAction-2", function () {
    it("SHOULD updateAction-2", async function () {
      const response = await session.updateAction(action1);
      expect(response).to.equal(true);
    });
  });
  describe("updateAction-3", function () {
    it("SHOULD updateAction-3", async function () {
      const response = await session.updateAction(action2);
      expect(response).to.equal(true);
    });
  });
  describe("findActions", function () {
    it("SHOULD findActions between entry 2(skip)-3(limit)", async function () {
      const response = await session.getActions(action0.uid, skip, limit);
      console.log({ response })
      expect(response).to.have.length(1);
    });
  });
  describe("countActions", function () {
    it("SHOULD countActions", async function () {
      const response = await session.countActions(uid);
      expect(response).to.equal(3);
    });
  });


});

// ------------------ '(◣ ◢)' ---------------------
