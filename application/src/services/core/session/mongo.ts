import * as mongoose from "mongoose";
import { handleError } from "../../../lib/errors/e";
import { logger } from "../../../lib/logger/winston";

import {  SessionModel } from "./interface";
import { SessionStore } from "./storage";

// -o_o===schema====================================================
const Schema = mongoose.Schema;

const SessionSchema = new Schema({
 genesis: {
  type: Number
 },
 ip: {
  type: String
 },
 device: {
   type: String
 },
 uid: {
  type: String,
  index: true
 },
 action: {
  type: String,
  index: true,
 },
 auth_level: {
  type: String,
 },
 reference: {
  type: String,
  index: true,
  unique: true
 }
});
// ------------------° ̿ ̿'''\̵͇̿̿\з=(◕_◕)=ε/̵͇̿̿/'̿'̿ ̿ °------------------
const session = mongoose.model("session", SessionSchema);

function MongoDocToSession(document: object): SessionModel {
 return {
  genesis: document['genesis'],
  uid: document['uid'],
  ip: document['ip'],
  device: document['device'],
  action: document['action'],
  auth_level: document['auth_level'],
  reference: document['reference']
 }
}
// ------------------° ̿ ̿'''\̵͇̿̿\з=(◕_◕)=ε/̵͇̿̿/'̿'̿ ̿ °------------------

export class MongoSessionStore implements SessionStore {
 async createSessionAction(action: SessionModel): Promise<boolean | Error> {
  try {
    const new_action = new session(action);

    const doc = await new_action.save();
    if (doc) 
     return true;
    else{
     logger.error({"CREATE SESSION ERROR!:": doc});
     return false;
    }

  } catch (e) {
   return handleError(e)
  }
 }
 async findActions(uid: string, skip: number, limit: number): Promise<Array<SessionModel> | Error>{
   try{
     const data = await session.find({uid})
     .sort({'genesis': -1})
     .skip(skip)
     .limit(limit)
     .exec();

     if(data){ 
       const actions = data.map((action)=>{
         return MongoDocToSession(action);
       });
       return actions;

     }
     else{
       return handleError({
         code: 404,
         message: "No session actions found."
       })
     }
   }
   catch(e){
     return handleError(e);
   }
 }
 async deleteSession(uid: string): Promise<boolean | Error>{
  try{
    const data = await session.deleteMany({uid})
    .exec();

    if(data){
      return true;

    }
    else{
      return handleError({
        code: 404,
        message: "No session actions found."
      })
    }
  }
  catch(e){
    return handleError(e);
  }
}
async countActions(uid: string): Promise<number | Error>{
  try{
    const data = await session.countDocuments({uid})
    .exec();

    if(data){
      return data as number;
    }
    else{
      return handleError({
        code: 404,
        message: "No session actions found."
      })
    }
  }
  catch(e){
    return handleError(e);
  }
}
}
// ------------------° ̿ ̿'''\̵͇̿̿\з=(◕_◕)=ε/̵͇̿̿/'̿'̿ ̿ °------------------

// ------------------ '(◣ ◢)' ---------------------
