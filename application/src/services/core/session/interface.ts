import {MFALevel} from "../auth/interface";

export interface SessionInterface{
 getActions(uid: string,skip: number,limit: number): Promise<Array<SessionModel> |Error>;
 updateAction(action: SessionModel): Promise<boolean | Error>;
}

export interface SessionModel{
 genesis?: number;
 uid?:string;
 ip?: string;
 device?: string;
 action?: ActionNames;
 code?: ActionCodes;
 auth_level?: MFALevel;
 reference?:string;
}

export enum ActionNames{
 "JWT"    = "Logged In",
 "XTFA"   = "Exchanged 2FA Secret",
 "VTFA"   = "Verified 2FA Secret",
 "BTCG"   = "Created Bitcoin Account",
 "BTCP"   = "Bitcoin Payment",
 "XBUY"   = "Stacked Sats",
 "SBK"    = "Sats Back",
 "ETXF"   = "Flagged Transaction",
 "ETXZ"   = "Failed Transaction",
 "CBTC"   = "Cold Card Payment Request"
}

export enum ActionCodes{
    "Logged In" = 1,
    "Exchanged TFA Secret" = 2,
    "Verified TFA Secret" = 3,
    "Created Bitcoin Account" = 4,
    "Bitcoin Payment" = 5,
    "Stacked Sats" = 6,
    "Sats Back" = 7,
    "Flagged Transaction" = 8,
    "Failed Transaction" = 9,
    "Cold Card Payment Request" = 10
}

