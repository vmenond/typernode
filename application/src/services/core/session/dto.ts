import { logger, r_500 } from "../../../lib/logger/winston";

import {
  filterError,
  respond,
  parseRequest,
} from "../../../network/handler";
import {
  AuthRequest, MFALevel
} from "../auth/interface";

import * as auth from "../auth/auth";
import * as sesh from "./session";
import { handleError } from "../../../lib/errors/e";

const access = new auth.S5Auth();
const session = new sesh.S5Session();


export async function sessionMiddleware(req, res, next) {
  try {
    const auth_options: AuthRequest = {
      headers: req.headers,
      mfa_level: MFALevel.B1,
    };

    const user = await access.authenticate(auth_options);
    if (user instanceof Error) {
      throw user;
    }

    req["user"] = user;
    next();


  } catch (e) {
    const result = filterError(e, r_500, {
      resource: req["originalUrl"],
      method: req["method"],
      ip: (req.headers["x-forwarded-for"]) ? (req.headers["x-forwarded-for"]) : req.ip
    });
    respond(result.code, result.message, res, req);
  }
}


export async function handleGetActions(req, res) {
  try {
    const request = sanitizeGetSessionActions(req);
    if(request instanceof Error) throw request;

    const session_actions = await session.getActions(request.user.uid, parseInt(request.query.skip), parseInt(request.query.limit));
    if (session_actions instanceof Error) {
      throw session_actions;
    }

    const message = {
      status: true,
      message: session_actions
    };
    respond(200, message, res, request);
  } catch (e) {
    const request = parseRequest(req);
    const result = filterError(e, r_500, request);
    respond(result.code, result.message, res, request);
  }
}
function sanitizeGetSessionActions(req){
  try{
    const request = parseRequest(req);
    const status400 = {
      body:{
        skip: true,
        limit: true
      }
    }
    if( parseInt(request.query.skip) >1000){
      status400.body.skip = false;
    }
    if(parseInt(request.query.limit) >21){
      status400.body.limit = false;
    }

    if(status400.body.limit && status400.body.skip)
    return request;
    else
    return handleError({
      code: 400,
      message: status400
    });
    
  }
  catch(e){
    return handleError(e);
  }
}

