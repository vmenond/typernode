import {SessionModel} from "./interface";

export interface SessionStore{
 createSessionAction(action: SessionModel): Promise<boolean|Error>;
 findActions(email: string, skip: number, limit: number): Promise<Array<SessionModel> | Error>;
 countActions(email:string):Promise<number | Error>;
}

