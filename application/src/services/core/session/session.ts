
import { handleError } from "../../../lib/errors/e";
import * as random from "../../../lib/uid/uid";

import { SessionInterface, SessionModel, ActionNames,ActionCodes} from "./interface";
import * as mongo from "./mongo";
import { S5Request } from "../../../network/handler";

const store = new mongo.MongoSessionStore();
const uid = new random.S5UID();



export class S5Session implements SessionInterface {
  async getActions(uid: string,skip: number,limit: number): Promise<Array<SessionModel> |Error>{
    const actions = await store.findActions(uid,skip,limit);
    if (actions instanceof Error) throw actions;

    actions.map(action=>{
      action.code = ActionCodes[action.action]
    });
    
    return actions;
  }
  async updateAction(action: SessionModel): Promise<boolean | Error>{
    action.genesis = Date.now();
    const status = await store.createSessionAction(action);
    return status;
  }
  async countActions(uid:string): Promise<number | Error>{
    return await store.countActions(uid);
  }
  createAction(request: S5Request,action: ActionNames, reference: string):SessionModel{
    return {
      ip: request.ip as string,
      device: request.device,
      uid: request.user.uid,
      action,
      reference,
      auth_level: request.user.auth_level
    }
  }
}
