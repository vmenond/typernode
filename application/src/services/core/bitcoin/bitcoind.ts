/*
api.sats.cc: aux: bitcoin-core rpc interface

Matrix Network International B.V.#
developed by(•̪●) == ε0O o o oo o o o o O1shi `(•.°)~#

*/
// -<..>===========================================================~|
const rp = require("request-promise");

const math = require("mathjs");
import { logger } from "../../../lib/logger/winston"
import { handleError } from "../../../lib/errors/e";

import * as config from "../../../config.json";
import {
  BitcoinInterface,
  S5BitcoinRecipient,
  BitcoinBalances,
  S5MinerFees,
  AddressType,
  NodeAuth,
  NodeMethods,
  XPubDescriptor,
  BitcoinFeeType
} from "./interface";



// const wallet = new S5Wallet();
const FAST_CONF = config.FAST_CONF;
const MED_CONF = config.MED_CONF;
const SLOW_CONF = config.SLOW_CONF;
const FEE_CONF = config.FEE_CONF_SETTING;
const FEE_MODE = "CONSERVATIVE";
const FEE_DENOM = 3.5;
// -o_o===PrivateCoreNode=================================================|

export const receiving_address = "3GHYj4KXMvvy42oiRdShZQFjGs9ZbuLndk";


// ---------- '(bitcoind-rpc-connect)' ------------

async function requestNode(
  auth: NodeAuth,
  method: string,
  params: Array<any>,
): Promise<any | Error> {
  try {
    const IP = process.env.BITCOIN_IP;
    const PORT = process.env.BITCOIN_PORT;

    const options = {
      method: "POST",
      url: "",
      headers: {
        "Content-Type": "application/json"
      },
      body: {},
      json: true
    };

    const encoded = Buffer.from(`${auth.rpc_auth}`).toString('base64');
    const node_url = (auth.wallet_name===("root"||"")) ? `http://${IP}:${PORT}/wallet/${auth.wallet_name}` : `http://${IP}:${PORT}/wallet/`
    
    console.log({auth});
    console.log({node_url});

    options.url = node_url;
    options.headers["Authorization"] = `Basic ${encoded}`;
    options.body["jsonrpc"] = "1.0";
    options.body["id"] = method;
    options.body["method"] = method;
    options.body["params"] = [];
    options.body["params"] = params;


    // console.log({options})
    const body = await rp(options);

    if (body === undefined) {
      return handleError("Recieved empty body from bitcoin.call()");
    } else if (body.result !== null) {
      return (body.result);
    } else if (body.status) {
      return (body);
    } else if (body.error !== null) {
      return handleError(body.error);
    }

  } catch (e) {
    if (e.error)
      return handleError(e.error);
    if (e.message)
      return handleError(e.message);
    else
      return handleError(e);
  }

}

// ------------------ '(◣_◢)' ---------------------


export class BitcoinCore implements BitcoinInterface {

  async unlockWallet(auth: NodeAuth): Promise<boolean | Error> {
    {
      try {
        // const load = await requestNode(auth, NodeMethods.LDW, [auth.wallet_name]);
        // if(load instanceof Error) return load;

        const unlock = await requestNode(auth, NodeMethods.WPASS, [auth.wallet_crypt, 10]);
        if (unlock instanceof Error) return unlock;

        return true;
      }
      catch (e) {
        return handleError(e);
      }
    }

  }

  async generateNewAddress(auth: NodeAuth, label: string, type: AddressType): Promise<string | Error> {
    let add_type;
    if(type===AddressType.SH) add_type='p2sh-segwit';
    else add_type = type;

    const result = await requestNode(auth, NodeMethods.GNA, [label, add_type]);
    if (result instanceof Error) return result;
    return (result as string);

  }
  async validateAddress(auth: NodeAuth, address: string): Promise<boolean | Error> {
    try {
      const result = await requestNode(auth, NodeMethods.VA, [address]);
      if (result instanceof Error) return result;
      // logger.verbose({ result });

      if (result['isvalid']) {
        return result['isvalid'];
      }
      else {
        return handleError({
          code: 400,
          message: result
        });
      }
    }
    catch (e) {
      return handleError(e);
    }
  }
  async listAddressGroupings(auth: NodeAuth): Promise<Array<any> | Error> {
    try {
      const result = await requestNode(auth, NodeMethods.LAG, []);
      return result;
    }
    catch (e) {
      return handleError(e);
    }
  }
  async getAddressInfo(auth: NodeAuth, address: string): Promise<Array<object> | Error> {
    try {
      const result = await requestNode(auth, NodeMethods.GAI, [address]);

      if (result instanceof Error) {
        if (JSON.parse(result.message)['error']['code'] === -5)
          return handleError("invalid");
      }
      return (result);
    } catch (e) {
      return handleError(e);
    }
  };
  async listReceivedAddresses(auth: NodeAuth): Promise<Array<any> | Error> {
    const result = await requestNode(auth, NodeMethods.LRA, []);
    if (result instanceof Error) return result;
    return result;
  }
  filterAddressType(address: string): AddressType | Error {
    if (address.startsWith('bc1') || address.startsWith('tb1')) return AddressType.B32
    if (address.startsWith('1') || address.startsWith('m') || address.startsWith('n')) return AddressType.PK
    if (address.startsWith('3') || address.startsWith('2')) return AddressType.SH
    else return handleError({
      code: 400,
      message: "Invalid Address Type"
    })
  }

  async getFees(auth: NodeAuth): Promise<S5MinerFees | Error> {
    {
      const high = await requestNode(auth, NodeMethods.ESF, [FAST_CONF, FEE_MODE]);
      if (high instanceof Error) return high;
      const mid = await requestNode(auth, NodeMethods.ESF, [MED_CONF, FEE_MODE]);
      if (mid instanceof Error) return mid;
      const low = await requestNode(auth, NodeMethods.ESF, [SLOW_CONF, FEE_MODE]);
      if (low instanceof Error) return low;

      const fees = {
        low: math.round(low['feerate'] as number /FEE_DENOM, 8),
        medium: math.round(mid['feerate'] as number /FEE_DENOM, 8),
        high: math.round(high['feerate'] as number /FEE_DENOM, 8)
      };
      return fees;

    }
  };
  async setTxFee(auth: NodeAuth, fee_amount: number): Promise<boolean | Error> {
    try {
      // fee amount in BTC/kb
      const result = await requestNode(auth, NodeMethods.STF, [fee_amount]);
      return result;

    } catch (e) {
      return handleError(e);
    }
  };
  async send(auth: NodeAuth, recipient: S5BitcoinRecipient): Promise<string | Error> {
    try {
      const info = await this.getNetworkInfo(auth);
      if(info instanceof Error) return info;


      console.log({ MAKING_BITCOIN_PAYMENT: recipient })
      let fee = FEE_CONF;
      switch(recipient.fees_type){
        case BitcoinFeeType.H:
          fee=FAST_CONF;
          break;
        case BitcoinFeeType.L:
          fee=SLOW_CONF; 
      }
      const replaceable=true;
      const params =  [
        recipient["address"],
        math.round(recipient["amount"], 8),
        recipient["from"],//comment
        recipient["to"],//comment_to
        false,
        replaceable,
        fee,
        FEE_MODE
      ];
      if(recipient.fees>0){
        params.push(FAST_CONF);
        params.push(FEE_MODE);
      }

      const result = await requestNode(
        auth,
        NodeMethods.SND,
        params,
      );
      if (result instanceof Error) {
        // check result.error.code
        // -28 = Verifying blocks ... 

        if (JSON.parse(result.message)['error']) {
          if (JSON.parse(result.message)['error']['code'] === -6)
            return handleError({
              code: 503,
              message: `!!Insufficient sats at hot wallet!!`
            })
        }
        else return result;


        return result;
      }

      logger.verbose({
        recipient,
        txid: result
      });
      const txid = result as string;
      return (txid);

    } catch (e) {

      return handleError(e);
    }
  };
  async sendRaw(auth: NodeAuth, hex: string): Promise<string | Error> {
    try {
      const result = await requestNode(auth, NodeMethods.SRTX, [hex]);

      return result;

    } catch (e) {

      return handleError(e);
    }
  }

  async getBalance(auth: NodeAuth): Promise<BitcoinBalances | Error> {
    const conf = await requestNode(auth, NodeMethods.GCB, []);
    if (conf instanceof Error)
      return conf;
    const unconf = await requestNode(auth, NodeMethods.GUB, []);
    if (unconf instanceof Error)
      return unconf;
    else {
      const confirmed = math.round(parseFloat(conf as string), 8);
      const unconfirmed = math.round(parseFloat(unconf as string), 8)
      return {
        confirmed, unconfirmed
      }

    }
  }
  async getHistory(auth: NodeAuth): Promise<Array<object> | Error> {
    try {
      const result = await requestNode(
        auth,
        NodeMethods.GH,
        [],
      );
      if (result instanceof Error) return result;
      // logger.verbose({result})  

      // logger.verbose({tx_parsed: respo})
      // 
      return (result);
    } catch (e) {
      return handleError(e);
    }
  };

  async localTxDetail(auth: NodeAuth, txid: string): Promise<object | Error> {
    try {
      const result = await requestNode(
        auth,
        NodeMethods.LTX,
        [txid]
      );
      if (result instanceof Error) {
        if (JSON.parse(result.message)['error']['code'] === -5) {
          return handleError({
            code: 403,
            message: "Non-wallet Txid"
          });
        }
        else
          return result;
      }// logger.verbose({result})  
      const rec_set = {
        txid: result["txid"],
        time: result["time"],
        fees: result["fee"],
        confirmations: result["confirmations"],
        blocktime: result["blocktime"],
        timestamp: result["timereceived"],
        from: result['comment'],
        to: result['to'],
        amount: result['amount'],
        tx_details: []
      };

      rec_set.tx_details = result["details"].filter(obj => {
        // a single txid can contain multiple incoming transactions. map through it!
        // !TEST! if checking against addresses is required
        // SHOULD NOT SINCE WE ARE USING gettransaction which only works for local wallet
        // remove this conditional to also notify about sends
        // console.log({tx_obj: obj});
        return {
          category: obj.category,
          address: obj.address,
          amount: obj.amount
        };
        // return (receives);
      });

      return (rec_set);

    } catch (e) {
      return handleError(e);
    }
  }
  async globalTxDetail(auth: NodeAuth, txid: string): Promise<object | Error> {
    try {
      const hex = await requestNode(
        auth,
        NodeMethods.GRTX,
        [txid],
      );
      if (hex instanceof Error) return hex;

      const response = await requestNode(
        auth,
        NodeMethods.DRTX,
        [hex],
      );

      return (response as Array<object>);
    } catch (e) {
      return handleError(e);
    }
  }
  async getNetworkInfo(auth: NodeAuth): Promise<object | Error>{
    try {
      const network_info = {
        traffic: BitcoinFeeType.L,
        mempool_size: 0
      }
      const s5fee =  await this.getFees(auth);
      if(s5fee instanceof Error) return s5fee;
   
      const raw_mempool = await requestNode(auth, NodeMethods.GRMP,[]);
      if (raw_mempool instanceof Error) return raw_mempool;
      
      network_info.mempool_size = raw_mempool.length;
      
      if (s5fee.high > 0.00012) network_info.traffic = BitcoinFeeType.M;
      if (s5fee.high > 0.00030) network_info.traffic =  BitcoinFeeType.H

      if(network_info.mempool_size>25000) network_info.traffic =  BitcoinFeeType.H

      if(network_info.mempool_size<25000) network_info.traffic =  BitcoinFeeType.M
      if(network_info.mempool_size<7000) network_info.traffic =  BitcoinFeeType.L



      return network_info;

    } catch (e) {
      return handleError(e);
    }
  }

  async createPSBT(auth: NodeAuth, recipient: S5BitcoinRecipient): Promise<object | Error> {
    try {
      const s5fee =  await this.getFees(auth);
      if(s5fee instanceof Error) return s5fee;

      let fee;
      switch(recipient.fees_type){
        case BitcoinFeeType.H:
          fee=s5fee.high * FEE_DENOM;
          break;
        case BitcoinFeeType.L:
          fee=s5fee.low * FEE_DENOM; 
      }

      const result = await requestNode(auth, NodeMethods.CPSBT,
        [
          [],
          {
            [recipient.address]: recipient.amount
          },
          0,// locktime
          {
            "includeWatching": true,
            "feeRate": recipient.fees || 0.00001000 // BTC/kb
          },
          true
        ]
      );

      if (result instanceof Error) {

        if (JSON.parse(result.message)['error']) {
          if (JSON.parse(result.message)['error']['code'] === -4)
            return handleError({
              code: 402,
              message: `Insufficient funds in wallet`
            })
        }
        else return result as object;

      }
      return result;

    } catch (e) {
      return handleError(e);
    }
  }
  async decodePSBT(auth: NodeAuth, psbt: string): Promise<object | Error> {
    try {
      const result = await requestNode(auth, NodeMethods.DPSBT, [
        psbt
      ]);

      return result;

    } catch (e) {
      return handleError(e);
    }
  }




  /**
   * [] 
   * {
   *  "2MyHHqjX3FsmFohG8kmcLU9oyRH6n1DttX3":0.003771
   * } 
   * 0 
   * {
   *  "changeAddress":"tb1qs49hqgnxzmvt76hvey796wplj37cympympkfrsrcgps3zwdt0ahs9vawvd\",
   *  "feeRate":0.0001,
   *  "subtractFeeFromOutputs":[0],
   *  "includeWatching":true
   * }
   */
  async createWallet(auth: NodeAuth, watch_only: boolean): Promise<boolean | Error> {
    {
      try {
        const result = await requestNode(auth, NodeMethods.CW, [auth.wallet_name, watch_only, false, auth.wallet_crypt]);

        return result;
      }
      catch (e) {
        return handleError(e);
      }
    }

  }

  createXPubDescriptor(
    fingerprint: string,
    derivation_path: string,
    xpub: string,
    client_checksum: string,
    change_checksum: string): XPubDescriptor {
    return {
      client: `wpkh([${fingerprint}${derivation_path}]${xpub}/0/*)#${client_checksum}`,
      change: `wpkh([${fingerprint}${derivation_path}]${xpub}/1/*)#${change_checksum}`
    }

  }
  /*
  [
  {
  "range": [0, 1000], 
  "timestamp": "now", 
  "keypool": true, 
  "watchonly": true, 
  "desc": "wpkh([8c13f027/84h/1h/1h]tpubDDQ6z9XNJ111Zzqh7oBdEJygpsRTtqPKvx1vb41oCBWWPYNzgZt8MeEoHEWdUwJia8JJ8G2R9LjpS8GmUXCUTUZvX7iaGj3NLeRB6gJb9sN/0/*)#j7vrd57w", 
  "internal": false
  }, 
  {
  "range": [0, 1000], 
  "timestamp": "now", 
  "keypool": true, 
  "watchonly": true, 
  "desc": "wpkh([8c13f027/84h/1h/1h]tpubDDQ6z9XNJ111Zzqh7oBdEJygpsRTtqPKvx1vb41oCBWWPYNzgZt8MeEoHEWdUwJia8JJ8G2R9LjpS8GmUXCUTUZvX7iaGj3NLeRB6gJb9sN/1/*)#r2fzspwk", 
  "internal": true
  }
  ]
  */
  async importXPub(auth: NodeAuth, desc: XPubDescriptor, range: Array<number>, watchonly: boolean): Promise<boolean | Error> {
    {
      try {
        const client = {
          range,
          timestamp: "now",
          keypool: true,
          watchonly,
          desc: desc.client,
          internal: false

        };
        const change = {
          range,
          timestamp: "now",
          keypool: true,
          watchonly,
          desc: desc.change,
          internal: true

        }
        const result = await requestNode(auth, NodeMethods.IXPB, [client, change]);

        return result;
      }
      catch (e) {
        return handleError(e);
      }
    }

  }


}

// ------------------ '(◣_◢)' ---------------------
