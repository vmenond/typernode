/*
api.sats.cc: auth_server routes: /bitcoin
Matrix Network International B.V.
#
developed by (•̪●)==ε0O o o oo o o o o O1shi`(•.°)~
*/
// ------------------ '(◣ ◢)' ---------------------
import { Router } from "express";
import { bitcoinMiddleware, handleInitPSBT,handleBroadcastTx  } from "./dto";
// ------------------ '(◣ ◢)' ---------------------
export const router = Router();
// ------------------ '(◣ ◢)' ---------------------
router.use(bitcoinMiddleware);
router.get("/tx/psbt",handleInitPSBT);
router.put("/tx/broadcast",handleBroadcastTx);

// put post should swap: put for update post for create
// ------------------° ̿ ̿'''\̵͇̿̿\з=(◕_◕)=ε/̵͇̿̿/'̿'̿ ̿ °------------------
