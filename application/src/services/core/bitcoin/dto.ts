import { logger, r_500 } from "../../../lib/logger/winston";
import { filterError, parseRequest, respond } from "../../../network/handler";
import { S5Slack } from "../../aux/slack/slack";
import { S5Vault } from "../../../lib/kms/vault";
import { BitcoinCore } from "./bitcoind";
import { NodeAuth, AddressType } from "./interface";

const kms = new S5Vault();
const bitcoin = new BitcoinCore();
const CC = "coldcard";
const HC = "";

export function bitcoinMiddleware(req, res, next) {
    try {
        if (req.headers['x-cc-secret'] || req.method === "POST" || req.method === "PUT")
            next();
        else throw ({
            code: 400,
            message: "Missing x-cc-secret header"
        })
    } catch (e) {
        const request = parseRequest(req);
        const result = filterError(e, r_500, request);
        respond(result.code, result.message, res, request);
    }
}


export async function handleInitPSBT(req, res) {
    try {
        const request = parseRequest(req);
        const pass = await kms.getCCPSBTPass();
        if (pass instanceof Error) return pass;


        const btc_rpc = await kms.getBitcoinRPCAuth();
        if (btc_rpc instanceof Error) return btc_rpc;
        const cold_wallet_pass = await kms.getBitcoinWalletAuth(CC);
        if (cold_wallet_pass instanceof Error) return cold_wallet_pass;
        const hot_wallet_pass = await kms.getBitcoinWalletAuth(HC);
        if (hot_wallet_pass instanceof Error) return hot_wallet_pass;

        const cauth: NodeAuth = {
            wallet_name: CC,
            wallet_crypt: cold_wallet_pass,
            rpc_auth: btc_rpc
        }
        const hauth: NodeAuth = {
            wallet_name: HC,
            wallet_crypt: hot_wallet_pass,
            rpc_auth: btc_rpc
        }

        if (req.headers['x-cc-secret'] !== pass) throw {
            code: 401,
            message: "Invalid CC Secret"
        };

        const fees = await bitcoin.getFees(cauth);
        if (fees instanceof Error) throw Error;
        let address = request.query.address;

        if (address === "node") {
            const recipient = await bitcoin.generateNewAddress(hauth, "cc-top-up", AddressType.B32);
            if (recipient instanceof Error) throw recipient;

            address = recipient;
        }
        const psbt = await bitcoin.createPSBT(cauth, {
            address,
            amount: request.query.amount,
            fees: fees.high
        });

        if (psbt instanceof Error) throw psbt;

        const decoded = await bitcoin.decodePSBT(cauth, psbt['psbt'] as string);
        if (decoded instanceof Error) throw decoded;

        psbt["decoded"] = decoded;
        respond(200, psbt, res, request);
    } catch (e) {

        const request = parseRequest(req);
        const result = filterError(e, r_500, request);

        respond(result.code, result.message, res, request);
    }
}

export async function handleBroadcastTx(req, res) {
    try {
        const request = parseRequest(req);
        if (request.query.hex.length < 64) throw {
            code: 400,
            message: "Invalid Tx Hex Format"
        }


        const btc_rpc = await kms.getBitcoinRPCAuth();
        if (btc_rpc instanceof Error) return btc_rpc;
        const wallet_pass = await kms.getBitcoinWalletAuth(CC);
        if (wallet_pass instanceof Error) return wallet_pass;

        const auth: NodeAuth = {
            wallet_name: CC,
            wallet_crypt: wallet_pass,
            rpc_auth: btc_rpc
        }

        const txid = await bitcoin.sendRaw(auth, request.query.hex);
        if (txid instanceof Error) throw txid;

        respond(200, txid, res, request);
    } catch (e) {

        const request = parseRequest(req);
        const result = filterError(e, r_500, request);

        respond(result.code, result.message, res, request);
    }
}