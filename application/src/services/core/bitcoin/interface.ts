/*
api.sats.cc: lib rpc: bitcoin interface

Matrix Network International B.V.#
developed by(•̪●) == ε0O o o oo o o o o O1shi `(•.°)~#

*/
export interface BitcoinInterface{
    createWallet(auth: NodeAuth, watchonly: boolean): Promise<boolean | Error>;
    unlockWallet(auth: NodeAuth): Promise<boolean | Error>;
    createXPubDescriptor(fingerprint: string,derivation_path: string,xpub: string,client_checksum:string,change_checksum:string): XPubDescriptor;
    importXPub(auth: NodeAuth, desc: XPubDescriptor,range: Array<number>,watchonly: boolean): Promise<boolean | Error>;
    
    generateNewAddress(auth: NodeAuth, label: string, type: AddressType): Promise<string | Error>;
    validateAddress(auth: NodeAuth, address: string): Promise<boolean | Error>;
    getAddressInfo(auth: NodeAuth, address: string): Promise<object | Error>;
    listReceivedAddresses(auth: NodeAuth): Promise<Array<string> | Error>;
    
    getFees(auth: NodeAuth) : Promise<S5MinerFees | Error>;
    setTxFee(auth: NodeAuth, fee: number): Promise<boolean | Error>;
    send(auth: NodeAuth, recipient: S5BitcoinRecipient, subtract_fee:boolean): Promise<string | Error>; 

    createPSBT(auth: NodeAuth, recipient: S5BitcoinRecipient):Promise<object | Error>;
    decodePSBT(auth: NodeAuth, psbt: string):Promise<object | Error>;
    sendRaw(auth: NodeAuth, hex: string): Promise<string | Error>;
    
    localTxDetail(auth: NodeAuth, txid: string): Promise<object | Error>;
    globalTxDetail(auth: NodeAuth, txid: string): Promise<object | Error>;
    getBalance(auth: NodeAuth) : Promise<BitcoinBalances | Error>;
    getHistory(auth: NodeAuth) : Promise<Array<object> | Error>;
    getNetworkInfo(auth: NodeAuth): Promise<object | Error>;
    filterAddressType(address: string): AddressType | Error;
}

export interface NodeAuth{
    wallet_name?:string;
    wallet_crypt?: string;
    rpc_auth?:string;  //uname:pass
}

export enum NodeMethods  {
    CW  = "createwallet",
    LDW  = "loadwallet",
    WPASS  = "walletpassphrase",
    LSW  = "listwalletdir",  
    GNA = "getnewaddress",
    VA  = "validateaddress",
    SND = "sendtoaddress",
    GCB  = "getbalance",
    GUB  = "getunconfirmedbalance",
    LTX = "gettransaction",
    GH  = "listtransactions",
    GAI = "getaddressinfo",
    LAG = "listaddressgroupings",
    GRTX = "getrawtransaction",
    DRTX = "decoderawtransaction",
    SRTX = "sendrawtransaction",
    ESF  = "estimatesmartfee",
    STF  = "settxfee",
    LRA  = "listreceivedbyaddress",
    IXPB  ="importmulti",
    CPSBT = "walletcreatefundedpsbt",
    DPSBT = "decodepsbt",
    GRMP  = "getrawmempool",
    GNI   = 'getnetworkinfo'

}

export enum AddressType{
    B32 = "bech32",
    PK  = "legacy",
    SH  = "p2sh"
}
export interface S5MinerFees{
    low: number;
    medium: number;
    high: number;
}

export interface S5BitcoinRecipient{
    address: string;
    amount: number;
    comment?: string;
    fees?: number; // BTC/kb
    fees_type?:BitcoinFeeType;
    txid?: string;
    to?:string;
    from?:string;
}
export enum BitcoinFeeType {
    "L" = "low",
    "M" = "medium",
    "H" = "high"
}
export interface BitcoinBalances {
    confirmed?: number;
    unconfirmed?: number;
};

export enum BitcoinTxDirection{
    IN  = "in",
    OUT = "out",
    X  = "x"
}


export interface XPubDescriptor{
    client?:string;
    change?:string;
}

  