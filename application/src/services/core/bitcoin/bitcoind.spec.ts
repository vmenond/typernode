/*
api.sats.cc: aux specifications
market_matrix
Matrix Network International B.V.
#
21
developed by (•̪●)==ε0O o o oo o o o o O1shi`(•.°)~#
*/
// ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------
// IMPORTS
import { expect, assert } from "chai";
import "../../../core/bitcoin/node_modules/mocha";
const sinon = require("sinon");
const math = require("mathjs");
import { logger } from "../../../lib/logger/winston";
import * as config from "../../../misc/test_config.json";
import * as s5bitcoin from "./bitcoind";
import { AddressType, NodeAuth } from "./interface";
import { S5Vault } from "../../../lib/kms/vault";

const bitcoin = new s5bitcoin.BitcoinCore();
const vault = new S5Vault();
// ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------
// GLOBAL CONFIGURATIONS
const allow_send = false || config.enable_bitcoin_send;
const recipient = {
  amount: 1 | config.bitcoin_send_amount,
  address: config.bitcoin_send_address
};
let txid = "769ad26df5697982a5e2961bbeffdfee602c4d09bcea1b58ef8f79789165537e";
const NAME = "0riginSeed"
const PASSWORD = "secretys"
console.log("BTC Node Test Settings: ", {
  allow_send,
  recipient
})

const n = 2;
const pub_keys = [
  '038395f09a8629d4265724918ba4148094c17dc85a0a61db6ed2c5113ff5c4fae7',
  '031acdb69d73e484f0f74a24dd4234b63947bfb598a6b2ae100529a271ec2dfa52',
  '02a357c9988fbee29a690021361bde969d6f44b637e98711a3cedbcf7e418025b1'
]
const addresses = [
  'tb1qr5y2my64c85h3newj963aa9duc0l8q885nv9ls',
  'tb1qwmke3pxqp3ddeqdhxnrkppdu3qgjxgpqrddx0c',
  'tb1ql0px0e2ss6zw85jqhsa3ukx9v2npzdery6ej55',
];
const priv_keys = [
  'cW2Jij9aroqHAjpc58q9DR9Q9uRTXdNpLN9Ubvx9CsCLUMeU2Gzk',
  'cVU97WLQT2UCYSmrRnD8CMkKJbe2ZmjoA8zAF4QxJkBjBvLoBkud',
  'cRf8Pk64qsaMywC6HN31n6YyxpN1JrPhjBLVxjLPXraZMdfcZ9h9',

];
let redeemScript;
let ms_address;
const green_ext = "2NDmeyMgGsukzVtu8gbeJ1m5aQURFVmXH5U";
let node_local_cc = "2NB6Y9Pbf1YDot5Bw2yx4zGMAmYG4kDJQ4o";

const user = {
  email: "kyc0@test.com",
  uid: "KVOTUQZFGJXGIQK3KNOVKYSYK4UEIKS"
}

const ms_recipient = {
  address: green_ext,
  amount: 0.000132010,
  fees: 0.00003000,
};

let psbt0;
let psbt1;
let psbt2;
let hex;
const fee_amount = 0.0012312;
const auth: NodeAuth = {
  wallet_name: "",
  wallet_crypt: "secret",
  rpc_auth: "user:secret"
}
const cauth: NodeAuth = {
  wallet_name: "coldcard",
  wallet_crypt: "secret",
  rpc_auth: "user:secret"
}
// ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------
// TESTS
describe("Initalizing Test: Auxilliary Services - bitcoind/bitcoin-cli RPC interface", function () {

  before(async function () {
    auth.rpc_auth = await vault.getBitcoinRPCAuth() as string;
    cauth.rpc_auth = auth.rpc_auth;
    auth.wallet_crypt = await vault.getBitcoinWalletAuth(auth.wallet_name) as string;
  });



  describe("unlockWallet", function () {
    it("SHOULD unlock bitcoin core wallet", async function () {
      const response = await bitcoin.unlockWallet(auth);
      console.log({response})
      expect(response).to.equal(true);
    });
  });

  describe("generateNewAddress(bech32)", function () {
    it("SHOULD return a bc1 address from a given watch only wallet name", async function () {

      const response = await bitcoin.generateNewAddress(cauth, user.uid, AddressType.B32);
      const type = bitcoin.filterAddressType(response as string);
      console.log({response})
      expect(type).to.equal(AddressType.B32);
    });
  });
 
  describe("generateNewAddress(p2sh)", function () {
    it("SHOULD return a bc1 address from a given watch only wallet name", async function () {

      const response = await bitcoin.generateNewAddress(cauth, user.uid, AddressType.SH);
      const type = bitcoin.filterAddressType(response as string);
      console.log({response})
      node_local_cc = response as string;
      expect(type).to.equal(AddressType.SH);
    });
  });


  describe("getBalance", function () {
    it("SHOULD return an balance given an xpub", async function () {
      const response = await bitcoin.getBalance(cauth);
      console.log({balance:response})

      expect(response).to.have.property('confirmed');
    });
  });
  describe("getHistory", function () {
    it("SHOULD list wallet txs history", async function () {
      const response = await bitcoin.getHistory(cauth);

      expect(response).to.be.a('array');
    });
  });

  describe("listAddressGroupings()", function () {
    it("SHOULD return a list of addresses with label and amounts received", async function () {
      const response = await bitcoin.listAddressGroupings(cauth);
      // console.debug(JSON.stringify({ response }, null, 2)); // parse it
      expect(response).to.be.a("array");

    });
  });
  describe("getAddressInfo(solvable)", function () {
    it("SHOULD return details of a local address", async function () {
      const response = await bitcoin.getAddressInfo(cauth, node_local_cc);
      // console.debug(JSON.stringify({ response }, null, 2)); // parse it

      expect(response).to.be.a("object");
      expect(response['labels'][0]).to.equal(user.uid);
      expect(response['solvable']).to.equal(true);

    });
  });
  describe("getAddressInfo(notmine)", function () {
    it("SHOULD return details of a local address", async function () {
      const response = await bitcoin.getAddressInfo(cauth, green_ext);
      // console.debug(JSON.stringify({ response }, null, 2)); // parse it

      expect(response).to.be.a("object");
      expect(response['solvable']).to.equal(false);

    });
  });


  describe("localTxDetail(txid)", function () {
    it("SHOULD return localTxDetail", async function () {
      const response = await bitcoin.localTxDetail(cauth, txid);
      expect(response).to.be.a("object");
      expect(response['txid']).to.equal(txid);

    });
  });

  describe("getFees()", function () {
    it("SHOULD get tx fee estimates", async function () {
      const response = await bitcoin.getFees(cauth);
      console.log({response});
      expect(response).to.have.property('high');
    });
  });



  describe("setTxFee()", function () {
    it("SHOULD set tx fee for the local node", async function () {
      const response = await bitcoin.setTxFee(auth, fee_amount);
      expect(response).to.equal(true);
    });
  });

  describe.skip("globalTxDetail(full-node)", function () {
    it("SHOULD return an object with tx details given any txid", async function () {
      const _txid = "ca24a16a477c06fdd56350e302440980a642c5fdcf7b44cff520e864ddc4eef5";
      const response = await bitcoin.globalTxDetail(auth, _txid);

      expect(response).to.be.a("object");
    });
  });


  describe("createPSBT", function () {
    it("SHOULD create a psbt", async function () {
      const response = await bitcoin.createPSBT(cauth,ms_recipient);
      console.log({ response })
      expect(response).to.have.property('psbt');
      expect(response).to.have.property('fee');
      psbt0 = response['psbt']
    });
  });

  describe("decodePSBT", function () {
    it("SHOULD decode a psbt", async function () {
      const response = await bitcoin.decodePSBT(cauth,psbt0);
      console.log({ response })

      expect(response).to.have.property('tx');
    });
  });
  describe("getNetworkInfo", function () {
    it("SHOULD get bitcoin network info", async function () {
      const response = await bitcoin.getNetworkInfo(cauth);
      console.log({ response })

      expect(response).to.have.property('traffic');
    });
  });
  if (allow_send) {
    describe.skip("send", function () {
      it("SHOULD return a txid", async function () {
        const response = await bitcoin.send(auth, recipient);
        txid = response as string;
        console.log({ response })
        expect(response).to.be.a("string");
      });
    });


    describe.skip("sendRaw", function () {
      it("SHOULD return a txid", async function () {
        const signed_hex = ""
        const response = await bitcoin.sendRaw(auth, signed_hex);
        txid = response as string;
        console.log({ response })
        expect(response).to.be.a("string");
      });
    });

  }
});
// ------------------ '(◣ ◢)' ---------------------
  /*
  {
    "tx_detail": {
      "txid": "4116ce38265d264a4d96b7f978eb721a7c4db212375670c4b3f572c1cc53b766",
      "time": 1586030202,
      "fees": -0.00000737,
      "confirmations": 0,
      "tx_details": [
        {
          "address": "34V2oyoqfTtPWAERW1kUqa3McfeiAN1ENh",
          "category": "send",
          "amount": -0.001,
          "label": "",
          "vout": 1,
          "fee": -0.00000737,
          "abandoned": false
        },
        {
          "address": "34V2oyoqfTtPWAERW1kUqa3McfeiAN1ENh",
          "category": "receive",
          "amount": 0.001,
          "label": "",
          "vout": 1
        }
      ]
    }
  }
  */