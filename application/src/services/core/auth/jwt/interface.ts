/*
interface: api.sats.cc/auth-jwt
#
developed by (•̪●)==ε0O o o oo o o o o O1shi`(•.°)~#
Matrix Network International B.V.
*/

export interface JWTInterface{
 connect(): Promise<boolean  | Error>;
 issue(payload: JWTPayload): Promise<object | string | Error>;
 verify(token: string): Promise<JWTUser | Error>;
}


export interface JWTUser {
 genesis?:number;
 firebase_uid?: string;
 email?: string;
 expiry?: number;
 secret?: string;
 uid?:string;
};
export interface JWTPayload{
 user: string;
}