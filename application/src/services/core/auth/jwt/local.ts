/*
#
api.sats.cc: auth_server lib: jwt
Matrix Network International B.V.
#
origins of the token master
developed by (•̪●)==ε0O o o oo o o o o O1shi`(•.°)~#
*/
// ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------
import jwt from "jsonwebtoken";
// ------------------ '(◣ ◢)' ----------------------
import { S5Vault } from "../../../../lib/kms/vault";
import { JWTInterface } from "./interface";
import { handleError } from "../../../../lib/errors/e";
import * as util from "util";

const kms = new S5Vault();

// ------------------ '(◣ ◢)' ----------------------
export class S5LocalJWT implements JWTInterface {
    async verify(request) {
        try {
            const private_key = await kms.unlockAppSecret("SATS_MFA_ACCESS_TOKEN_SECRET");
            let token =
                request.headers["x-access-token"] || request.headers["authorization"]; // Express headers are auto converted to lowercase
            if (token.startsWith("Bearer ")) {
                // Remove Bearer from string
                token = token.slice(7, token.length);
            }

            if (token) {
                const result = jwt.verify(token,private_key);
                console.log({result});
                const decoded = jwt.decode(token);
                console.log({decoded})
                if (result instanceof Error) {
                    return handleError({
                        code: 401,
                        message: "Invalid token"
                    });
                } else {
                    return (decoded);
                }

            } else {
                return handleError({
                    code: 401,
                    message: "Invalid token"
                });
            }
        } catch (e) {
            if(e.message==="jwt malformed" || e.message==="jwt expired")
            return handleError({
                code: 401,
                message: "Invalid token"
            });

            return handleError(e);
        }

    }
    // ------------------ '(◣ ◢)' ----------------------
    async issue(payload) {
        try {
            const private_key = await kms.unlockAppSecret("SATS_MFA_ACCESS_TOKEN_SECRET");
            const token = jwt.sign(
                {
                    payload
                },
                private_key,
                {
                    expiresIn: 60 * 60
                }
            );
            
            return (token);

        } catch (e) {
            return handleError(e);
        }

    }
    async connect() {
        try {
            return false;
        } catch (e) {
            handleError(e)
        }
    }
    // ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------
}

// ------------------ ̿ ̿'''\̵͇̿̿\з=(◕_◕)=ε/̵͇̿̿/'̿'̿ ̿ °------------------