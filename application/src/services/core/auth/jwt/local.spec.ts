/*
firebase specification: api.sats.cc/auth
#
developed by (•̪●)==ε0O o o oo o o o o O1shi`(•.°)~#
Matrix Network International B.V.
*/

import { expect } from "chai";
import "../../bitcoin/node_modules/mocha";

const sinon = require("sinon");

import * as local from "./local";
import * as  kms from "../../../../lib/kms/vault";

const jwt = new local.S5LocalJWT();
let token = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjIzNzA1ZmNmY2NjMTg4Njg2ZjhhZjkyYWJiZjAxYzRmMjZiZDVlODMiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vc2F0cy1jYyIsImF1ZCI6InNhdHMtY2MiLCJhdXRoX3RpbWUiOjE2MDExMzc1NTgsInVzZXJfaWQiOiIxWFNRbTJuMHB0UkllNmtrZTdPRWdqaTRQV20xIiwic3ViIjoiMVhTUW0ybjBwdFJJZTZra2U3T0Vnamk0UFdtMSIsImlhdCI6MTYwMjQ5NjY0OCwiZXhwIjoxNjAyNTAwMjQ4LCJlbWFpbCI6Imt5YzBAdGVzdC5jb20iLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsiZW1haWwiOlsia3ljMEB0ZXN0LmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6InBhc3N3b3JkIn19.WNjgtDEB5JhyndWlxuZJURPCm7_6Kl2fJiMmt31JhyHr6YuyAIiGgiHAPvK6s8_zE2JsWNFLX80prVRjmIMs-8L03pzqj_3tXfk4JDTzPoc1fy3vCvO1EmDnvxXQNQNz-EVXN652trGxK3Q39TjP3HBAFkD6XvfmSPPQdOfDG1YKySGrRSkfNRVDx6S5BorcF8ybNMqn9blK3-EglyZQAah6rHfNRg7AqTJX_u5SdIFnuQ1wTvgTPKfG3si-XwT6L3DYzlav9A67PlG_Ym4cojfhIBdH9VaHiC4Qjh8JJkxb6OqF_6STUGxTNTTngaTnkrlp9rGGn5hh3XU-XwJjGQ";
const email = "kyc0@test.com"
const payload = {
    user: "vm",
}
// ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------
// GLOBAL CONFIGURATIONS
// ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------
describe("Initalizing Test: Local JWT ", function () {
    const sandbox = sinon.createSandbox();
    before(async function () {
        sandbox.stub(kms.S5Vault.prototype, 'unlockAppSecret').resolves("shhhhhhhhhhhhhhhhhh");

    });
    describe("issue", function () {
        it("SHOULD issue a jwt", async function () {
            const response = await jwt.issue(payload);
            token = response as string;
            expect(response).to.be.a("string");
        });
    });

    describe("verify", function () {
        it("SHOULD verify a token", async function () {
            const request = {
                headers: {
                    "x-access-token": token
                }
            };
            const response = await jwt.verify(request);
            expect(response['payload']['user']).to.equal(payload.user);

        });
    });

});

// ------------------ '(◣ ◢)' ---------------------
