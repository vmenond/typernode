/*
controller: api.sats.cc/auth
#
developed by (•̪●)==ε0O o o oo o o o o O1shi`(•.°)~#
Matrix Network International B.V.
*/
import { handleError } from "../../../lib/errors/e";
import { logger } from "../../../lib/logger/winston"
import * as speakeasy from "./totp/speakeasy";
import { AuthInterface } from "./interface";
import { S5RequestHeaders } from "../../../network/handler";
import { AuthResponse, MFALevel } from "./interface";
import { S5LocalJWT } from "./jwt/local";

const jwt = new S5LocalJWT();
const totp = new speakeasy.S5SpeakEasy();

export class S5Auth implements AuthInterface {
  init() {
    return jwt.connect();
  }
  async authenticate(request) {
    const user = await basicAuth(request.headers);
    if (user instanceof Error) return user;

    switch (request.mfa_level) {
      case MFALevel.B1:
        return user;



      case MFALevel.T2:

        const totp_status = await totpAuth(request.headers, user.email);
        if (totp_status instanceof Error) return totp_status;

        return user;
     
      default:
        return handleError(`Unrecognized mfa_level: ${request.mfa_level}`);
    }
  }

}
// ------------------ '(◣ ◢)' ---------------------
export async function basicAuth(request_headers: S5RequestHeaders): Promise<AuthResponse | Error> {

  if (request_headers["authorization"]) {
    try {
      // split could throw an Error
      const token = request_headers["authorization"].split(" ")[1];

      const user = await jwt.verify(token);
      if (user instanceof Error) return user;

      let uid = "change";
  
      return {
        uid,
        email: user.email,
        expiry: user.expiry,
        firebase_uid: user.firebase_uid,

      };
    }
    catch (e) {
      logger.error({ handlers_basic_auth: e });
      return handleError({
        code: 400,
        message: "Badly Formatted Authorization Header. Must be in the format: Bearer jwtToken"
      })
    }
  } else
    return handleError({
      code: 400,
      message: "Missing Authorization header."
    });
}
// ------------------ '(◣ ◢)' ---------------------
async function totpAuth(request_headers: S5RequestHeaders, email: string): Promise<boolean | Error> {
  if (request_headers["x-sats-totp"]) {
    const status = await totp.verify(
      email,
      request_headers["x-sats-totp"]
    );
    if (status instanceof Error) return status;
    return true;
  } else {
    return handleError({
      code: 400,
      message: `Missing x-sats-totp header`
    });
  }
}
// ------------------ '(◣ ◢)' ---------------------

// ------------------ '(◣ ◢)' ---------------------
