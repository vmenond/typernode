/*
router: api.sats.cc/auth
#
developed by (•̪●)==ε0O o o oo o o o o O1shi`(•.°)~#
Matrix Network International B.V.
*/
// ------------------ '(◣ ◢)' ---------------------
import { Router } from "express";
import{
handleGetTotpSecret,
handlePostTotp,
authMiddleware,
handleDeleteTotp
} from "./dto"

// ------------------ '(◣ ◢)' ---------------------
export const router = Router();
// ------------------ '(◣ ◢)' ---------------------
router.use(authMiddleware);
router.get("/tfa", handleGetTotpSecret);
router.post("/tfa", handlePostTotp);
router.delete("/tfa", handleDeleteTotp);


// ------------------° ̿ ̿'''\̵͇̿̿\з=(◕_◕)=ε/̵͇̿̿/'̿'̿ ̿ °------------------
