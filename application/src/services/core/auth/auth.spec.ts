/*
controller specification: api.sats.cc/auth
Matrix Network International B.V.
developed by (•̪●)==ε0O o o oo o o o o O1shi`(•.°)~#
*/

import "mocha";
import { expect } from "chai";

const sinon = require("sinon");
import speakeasy from "speakeasy";
import * as kms from "../../../lib/kms/vault";

import { MongoDatabase } from "../../../storage/mongo";
import { S5Auth } from "./auth";
import { MFALevel } from "./interface";
import { DbConnection } from "../../../storage/interface";

const oth = new S5Auth();
const uid = "KVOTUQZFGJXGIQK3KNOVKYSYK4UEIKS"
const token = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjY5NmFhNzRjODFiZTYwYjI5NDg1NWE5YTVlZTliODY5OGUyYWJlYzEiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vc2F0cy1jYyIsImF1ZCI6InNhdHMtY2MiLCJhdXRoX3RpbWUiOjE2MDc2ODAzNTYsInVzZXJfaWQiOiIxWFNRbTJuMHB0UkllNmtrZTdPRWdqaTRQV20xIiwic3ViIjoiMVhTUW0ybjBwdFJJZTZra2U3T0Vnamk0UFdtMSIsImlhdCI6MTYwNzY4MDM1OCwiZXhwIjoxNjA3NjgzOTU4LCJlbWFpbCI6Imt5YzBAdGVzdC5jb20iLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsiZW1haWwiOlsia3ljMEB0ZXN0LmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6InBhc3N3b3JkIn19.YVYclV2n8h5BheeCpf9yRrM8PTXlxNZnN5WaE3GYOKHO3XV9iJ2k7C7aPzu8Xnw82H5AE1dMFetEOS_qWmjI0De7MqXmx8MecqGbdlwfT13_i7AkvTovikJGTlqyq4PJGbZbR3pC5YxrsEgm6Rvs24f6__ue2emt6WZg-2eLF09s9NWsTRFf_kgZr1_mgTf1sqj4vi2SVfBNyjv2p8gc_-UbG8voWuZAumwEXWxfnh3e1Yc7bSd-3V3b_iizYZbNVGnfowKOGQakvoWMLtkysWWtfwTMgsgBN8qcYrzwuEOzFLBtdfVnnQ6uj4ds3gHH6JAdpu5HA0iE1kkElOEObw";
const secret = "JY4DYKCENNNUY4TRJAUTM63OFFZC4JKQF4YWOZCRJJYVWRDRH5GA";
let otp;

const b_request = {
  headers: {
    "authorization": `Bearer ${token}`,
  },
  mfa_level: MFALevel.B1
}
const p_request = {
  headers: {
    "authorization": `Bearer ${token}`,
    "x-sats-pin": "23ed2e5b13d05294eadf1c55b856572dad24909e732bbd2eb24740934c8e4d18",
  },
  mfa_level: MFALevel.P2
}
const t_request = {
  headers: {
    "authorization": `Bearer ${token}`,
    "x-sats-totp": "545321",
    "x-sats-pin": "23ed2e5b13d05294eadf1c55b856572dad24909e732bbd2eb24740934c8e4d18",

  },
  mfa_level: MFALevel.T2
}
const db = new MongoDatabase();
// ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------
// GLOBAL CONFIGURATIONS


function generateTOTP(secret) {
  return {
    token: speakeasy.totp({
      secret,
      encoding: "base32",
      window: 1,
      step: 30
    }),
    remaining: 90 - Math.floor((new Date().getTime() / 1000.0) % 90)
  };
};
// ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------
describe("Initalizing Test: Auth Module ", function () {
  const sandbox = sinon.createSandbox();
  before(async function () {

    const connection: DbConnection = {
      port: process.env.DB_PORT,
      ip: process.env.DB_IP,
      name: 'sats',
      auth: 'auth_server:supersecret',
    };

    sandbox.stub(kms.S5Vault.prototype, 'allowService').resolves(true);
    const gsec_stubby = sandbox.stub(kms.S5Vault.prototype, "unlockAppSecret");


    await db.connect(connection);
  });

  after(function (done) {
    sandbox.restore();
    done();
  });

  describe("init", function () {
    it("SHOULD initialize s5 auth", async function () {
      const response = await oth.init();
      expect(response).to.equal(true);
    });
  });

  describe("basic authentication", function () {
    it("SHOULD authenticate a jwt token issued by firebase", async function () {
      const response = await oth.authenticate(b_request);
      if (response instanceof Error) throw response;
      expect(response['email']).to.be.a('string');
    });
  });
  describe("pin authentication", function () {
    it("SHOULD authenticate a user pin", async function () {
      const response = await oth.authenticate(p_request);
      if (response instanceof Error) throw response;
      console.log({ response })
      expect(response['uid']).to.be.a('string');
    });
  });
  describe("tfa authentication", function () {
    it("SHOULD authenticate a user 2fa token", async function () {
      t_request.headers["x-sats-totp"] = await generateTOTP(secret)['token'];
      const response = await oth.authenticate(t_request);
      if (response instanceof Error) throw response;
      expect(response['email']).to.be.a('string');
    });
  });



});

// ------------------ '(◣ ◢)' ---------------------
