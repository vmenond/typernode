/*
dto: api.sats.cc/auth
#
developed by (•̪●)==ε0O o o oo o o o o O1shi`(•.°)~#
Matrix Network International B.V.
*/
import { logger, r_500 } from "../../../lib/logger/winston";

import { respond, parseRequest, filterError } from "../../../network/handler"
import { handleError } from "../../../lib/errors/e";

import {
  AuthRequest, MFALevel, avatar_list
} from "./interface";
import { S5Auth } from "./auth";

import { S5SpeakEasy } from "./totp/speakeasy";

import { S5Session } from '../session/session';
import { ActionNames } from "../session/interface";
import { S5Slack } from "../../aux/slack/slack";

const session = new S5Session();
const access = new S5Auth();
const totp = new S5SpeakEasy();
const admin  = new S5Slack();

export async function authMiddleware(req, res, next) {
  const request = parseRequest(req);

  try {
    const auth_options: AuthRequest = {
      headers: req.headers,
      mfa_level: MFALevel.P2,
    };

    if(req.originalUrl=="/auth/pin") auth_options.mfa_level=MFALevel.B1;
    
    const user = await access.authenticate(auth_options);
    if (user instanceof Error) {
      throw user;
    }

    req["user"] = user;
    next();
  }
  catch (e) {
    const result = filterError(e, r_500, request);
    respond(result.code, result.message, res, request);
  }
}


export async function handleGetTotpSecret(req, res) {
  const request = parseRequest(req);

  try {
    const secret = await totp.setup(request.user.email);
    if (secret instanceof Error) throw secret;

        
    session.updateAction(session.createAction(request,ActionNames.XTFA,"SpeakEasy"))
    .catch(e=>logger.error({e}));
    
    const message = {
      status: true,
      message: { secret: secret.secret_b32 }
    }
    respond(200, message, res, request);
  }
  catch (e) {
    const result = filterError(e, r_500, request);
    respond(result.code, result.message, res, request);
  }
}

export async function handlePostTotp(req, res) {
  const request = parseRequest(req);

  try {
    if (request.headers['x-sats-totp']) {
      const status = await totp.verify(request.user.email, request.headers['x-sats-totp']);

      session.updateAction(session.createAction(request,ActionNames.VTFA,"SpeakEasy"))
      .catch(e=>logger.error({e}));

      if (status instanceof Error) throw status;
      const message = {
        status: true,
        message: true
      }
      respond(200, message, res, request);
    }
    else {
      const message = {
        status: false,
        message: "Missing x-sats-totp header"
      };
      respond(400, message, res, request);
    }

  }
  catch (e) {
    const result = filterError(e, r_500, request);
    respond(result.code, result.message, res, request);
  }
}



export async function handleDeleteTotp(req, res) {
  const request = parseRequest(req);

  try {
        
    await admin.message('2FA RESET REQUEST',request.user.email).catch((e=>{
      throw e;
    }));
    
    const message = {
      status: true,
      message: "Notified support. Please check your email."
    }
    respond(200, message, res, request);
  }
  catch (e) {
    const result = filterError(e, r_500, request);
    respond(result.code, result.message, res, request);
  }
}
