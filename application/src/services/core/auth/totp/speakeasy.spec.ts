/*
speakeasy specification: api.sats.cc/auth
#
developed by (•̪●)==ε0O o o oo o o o o O1shi`(•.°)~#
Matrix Network International B.V.
*/

import { expect } from "chai";
import "mocha";

const sinon = require("sinon");
import speakeasy from "speakeasy";

import {MongoDatabase}  from "../../../../storage/mongo";
import {S5SpeakEasy} from "./speakeasy";
import * as kms from "../../../../lib/kms/vault";
import { DbConnection } from "../../../../storage/interface";

const totp = new S5SpeakEasy();
const email = "kyc0@test.com";
const db = new MongoDatabase();
// ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------
// GLOBAL CONFIGURATIONS


function generateTOTP (secret) {
 return {
   token: speakeasy.totp({
     secret,
     encoding: "base32",
     window: 1,
     step: 30
   }),
   remaining: 30 - Math.floor((new Date().getTime() / 1000.0) % 30)
 };
};
let otp;
let secret = "KVOTUQZFGJXGIQK3KNOVKYSYK4UEIKS6EF3W6ZRYKQUGQJKUNVYA";
// ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------
describe("Initalizing Test: SpeakEasy ", function () {
 const sandbox = sinon.createSandbox();
 before(async function () {

  const connection: DbConnection = {
    port: process.env.DB_PORT,
    ip: process.env.DB_IP,
    name: 'sats',
    auth: 'auth_server:supersecret',
  }

  sandbox.stub(kms.S5Vault.prototype,'allowService').resolves(true);
  await db.connect(connection);
 });
 describe("setup", function () {
  it("SHOULD create a new totp entry", async function () {
   const response = await totp.setup(email);
   if(response instanceof Error) throw response;
   secret = response.secret_b32;
   console.log({secret})
   otp=generateTOTP(response.secret_b32);
   expect(response['secret_b32']).to.be.a('string');
  });
 });

 describe("verify", function () {
  it("SHOULD verify a totp against a local secret", async function () {
   const response = await totp.verify(email, otp.token);
   expect(response).to.equal(true);
  });
 });

 describe("getStatus()", function () {
  it("SHOULD get totp auth status of a user", async function () {
   const response = await totp.getStatus(email);

   expect(response).to.equal(true);
  });
 });

 describe("generateTOTP()", function () {
  it("SHOULD get totp auth status of a user", async function () {
   const response = await totp.generateTOTP(secret);
   expect(response['token']).to.equal(otp.token);
  });
 });

 describe.skip("remove()", function () {
  it("SHOULD remove totp secret of a user", async function () {
   const response = await totp.remove(email, "gingerbeeerrmmm");
   expect(response).to.equal(true);
  });
 });



});

// ------------------ '(◣ ◢)' ---------------------
