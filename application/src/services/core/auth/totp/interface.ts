/*
interface: api.sats.cc/auth-totp
#
developed by (•̪●)==ε0O o o oo o o o o O1shi`(•.°)~#
Matrix Network International B.V.
*/
export interface TOTPInterface{
 setup(email: string): Promise<TOTPSecret | Error>;
 verify(email: string, otp: string): Promise<boolean | Error>;
 getStatus(email: string): Promise<boolean| Error>;
 remove(email: string, password: string):Promise<boolean | Error>;
 generateTOTP(secret_b32: string): Promise<TOTPToken | Error>;
}

export interface TOTPSecret {
 genesis?: number;
 email?: string;
 secret_b32?: string;
 exp?:number;
 avatar?:string;
};
export interface TOTPToken{
 token: string,
 remaining: number;
}