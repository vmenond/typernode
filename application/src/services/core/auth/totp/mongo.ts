/*
tfa model: api.sats.cc/auth
#
developed by (•̪●)==ε0O o o oo o o o o O1shi`(•.°)~#
Matrix Network International B.V.
*/
// ---------------- ┌∩┐(◣_◢)┌∩┐ -----------------
import { handleError } from "../../../../lib/errors/e";
import mongoose, { DocumentProvider } from "mongoose";
import {TOTPSecret} from "./interface";
import {TOTPStore} from "./storage"
// ---------------- ┌∩┐(◣_◢)┌∩┐ -----------------

const tfa_schema = new mongoose.Schema(
  {
    genesis: {
      type: Number
    },
    email: {
      type: String,
      unique: true,
      required: true,
      index: true
    },
    secret_b32: {
      type: String,
      unique: true,
      required: true,
    },
    exp:{
      type: Number,
      default: 0
    },
    avatar:{
      type: String,
      default: 'none'
    }
  },
  {
    strict: true
  }
);
// ------------------ '(◣ ◢)' ---------------------
const tfa = mongoose.model("tfa", tfa_schema);
// ------------------ '(◣ ◢)' ---------------------
export class MongoTOTPStore implements TOTPStore{
  async create(user: TOTPSecret): Promise<boolean | Error>{
    try {
      const unique = await this.findOne(user.email);
      // console.log({ status });
      if (unique instanceof Error) {
        if(unique.name==="404"){
  
          const new_tfa = new tfa(user);
  
          const doc = await new_tfa.save();
          if (doc instanceof mongoose.Error) {
            return handleError(doc);
          } else {
            return true;
          }
        }
        else
        return unique;
      }

      else if (unique.secret_b32==="none"){
        return this.updateSecret(user.email,user.secret_b32);
      }
      else{
        return handleError({
          code:409,
          message:"Secret Shared"
        });
      }

    } catch (e) {
      return handleError(e);
    }
  }

  async findOne(email: string): Promise<TOTPSecret | Error>{
    try {
      const doc = await tfa.findOne({ email }).exec();
  
      if (doc) {
        if (doc instanceof mongoose.Error) {
          return handleError(doc);
        }
        
        const out: TOTPSecret = {
          genesis: doc["genesis"],
          email: doc["email"],
          secret_b32: doc["secret_b32"],
          exp:doc['exp'],
          avatar: doc['avatar']
        };
        return out;
      } else {
        // no data from findOne
        return handleError({
          code: 404,
          message: `No TFA entry`
        });
      }
    } catch (e) {
      return handleError(e);
    }
  }

  async updateExp(email: string, exp: number): Promise<boolean | Error>{
    try {
      const doc = await tfa.findOneAndUpdate({ email },{$set:{exp}}).exec();
  
      if (doc) {
        const err = doc.validateSync();
        if (err instanceof mongoose.Error) {
          return handleError(err);
        }

        return true;
      } else {
        // no data from findOne
        return handleError({
          code: 404,
          message: `No TFA entry for ${email}`
        });
      }
    } catch (e) {
      return handleError(e);
    }
  }

  async setAvatar(email: string, avatar: string): Promise<boolean | Error>{
    try {
      const doc = await tfa.findOneAndUpdate({ email },{$set:{avatar}}).exec();
  
      if (doc) {
        const err = doc.validateSync();
        if (err instanceof mongoose.Error) {
          return handleError(err);
        }

        return true;
      } else {
        // no data from findOne
        return handleError({
          code: 404,
          message: `No TFA entry for ${email}`
        });
      }
    } catch (e) {
      return handleError(e);
    }
  }

  async deleteSecret(email: string){
    try {
      const status = await tfa.findOneAndUpdate({email},{$set: {secret_b32:"none"}}).exec();
  
      if (status) {
        const err = status.validateSync();
        if (err instanceof mongoose.Error) {
          return handleError(err);
        } else {
          return true;
        }
      } else {
        return handleError({
          code: 404,
          message: "No entry for this user."
        });
      }
    } catch (e) {
      return handleError(e);
    }
  }
  async updateSecret(email: string, secret_b32:string){
    try {
      const status = await tfa.findOneAndUpdate({email},{$set: {secret_b32}}).exec();
  
      if (status) {
        const err = status.validateSync();
        if (err instanceof mongoose.Error) {
          return handleError(err);
        } else {
          return true;
        }
      } else {
        return handleError({
          code: 404,
          message: "No entry for this user."
        });
      }
    } catch (e) {
      return handleError(e);
    }
  }
  
}

// ------------------ '(◣ ◢)' ---------------------

export async function ensureUnique(email: string): Promise<boolean | Error> {
  try {
    const doc = await tfa.findOne({ email }).exec();
    // console.log(data);
    if (doc === null) return true;

    if (doc) {
      const err = doc.validateSync();
      if (err instanceof mongoose.Error) {
        return handleError(err);
      }

      return handleError({
        code: 409,
        message:
          "Exists"
      });
    } else return true;
  } catch (e) {
    return handleError(e);
  }
}

// ------------------ '(◣ ◢)' ---------------------

// ------------------° ̿ ̿'''\̵͇̿̿\з=(◕_◕)=ε/̵͇̿̿/'̿'̿ ̿ °------------------

// ------------------° ̿ ̿'''\̵͇̿̿\з=(◕_◕)=ε/̵͇̿̿/'̿'̿ ̿ °------------------