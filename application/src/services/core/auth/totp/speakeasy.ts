/*
api.sats.cc/auth-speakeasy
#
developed by (•̪●)==ε0O o o oo o o o o O1shi`(•.°)~#
Matrix Network International B.V.
*/

import speakeasy from "speakeasy";
import { handleError } from "../../../../lib/errors/e";
import * as mongo from "./mongo";
import { TOTPInterface, TOTPSecret, TOTPToken } from "./interface";
import * as vault from "../../../../lib/kms/vault";
import * as s5c from "../../../../lib/crypto/crypto";

const crypto = new s5c.S5Crypto();
const kms = new vault.S5Vault();
const store = new mongo.MongoTOTPStore();
const ZONE_TIME_IN_MINUTES = 5;

export class S5SpeakEasy implements TOTPInterface {
  async setup(email: string): Promise<TOTPSecret | Error> {
    try {
      const OPTIONS = {
        length: 32,
        name: "sats.cc"
      };

      const secret = speakeasy.generateSecret(OPTIONS);
      const secret_b32 = secret.base32;

      const entry: TOTPSecret = {
        genesis: Date.now(),
        email: email,
        secret_b32
      };

      const key = await kms.getTFAKey();
      if (key instanceof Error) {
        return key;
      }

      const entry_crypt = encryptSecret(entry, key);
      if (entry_crypt instanceof Error) return entry_crypt;

      const status = await store.create(entry_crypt);
      if (status instanceof Error) return status;

      return entry;

    } catch (e) {
      return handleError(e);
    }
  }

  async verify(email: string, otp: string): Promise<boolean | Error> {
    const entry_crypt = await store.findOne(email);
    if (entry_crypt instanceof Error) {
      if (entry_crypt.name === "404") {
        entry_crypt.name = "403"
      }
      return entry_crypt;
    }
    if(entry_crypt.secret_b32==="none") return handleError({
      code: 404,
      message: "No secret shared with user."

    });

    const key = await kms.getTFAKey();
    if (key instanceof Error) {
      return key;
    }

    const entry = decryptSecret(entry_crypt, key);
    
    if (entry instanceof Error) return entry;


    const status = speakeasy.totp.verify({
      secret: entry["secret_b32"],
      encoding: "base32",
      token: otp,
      window: 1,
      step: 30
    });
    if (status) {
      const exp = Date.now() + (60000 * ZONE_TIME_IN_MINUTES);
      const result = await store.updateExp(email, exp);
      if (result instanceof Error) return result;

      return true;
    }
    else return handleError({
      code: 403,
      message: "InvalidTOTP"
    });
  }

  async getStatus(email: string): Promise<boolean | Error> {
    const totp_model = await store.findOne(email);
    if (totp_model instanceof Error) {
      if (totp_model.name === "404") return false;
      else
      return totp_model;
    }
    else if(totp_model['secret_b32']==='none')return false;
    else return true;
  }

  async getAvatar(email: string): Promise<string | Error> {
    const totp_model = await store.findOne(email);
    if (totp_model instanceof Error) {
      if (totp_model.name === "404") return "none";
      return totp_model;
    }
    if(totp_model.avatar==='none') return 'none';

    const key = await kms.getTFAKey();
    if (key instanceof Error) {
      return key;
    }

    const avatar = crypto.decryptAESMessageWithIV(totp_model.avatar, key);
    if (avatar instanceof Error) return avatar;

    return avatar;
  }
  async setAvatar(email: string,avatar: string): Promise<boolean | Error> {

    const key = await kms.getTFAKey();
    if (key instanceof Error) {
      return key;
    }

    const avatar_crypt = crypto.encryptAESMessageWithIV(avatar, key);
    if (avatar_crypt instanceof Error) return avatar_crypt;

    const totp_model = await store.setAvatar(email,avatar_crypt);
    if (totp_model instanceof Error) {
      if (totp_model.name === "404") return false;
      else
      return totp_model;
    }
    return true;
  }
  async remove(email: string, password: string): Promise<boolean | Error> {
    const p_status = await kms.allowService("delete_tfa", password);
    if (p_status instanceof Error) {
      return p_status;
    }

    const status = await store.deleteSecret(email);

    return status;

  }
  async generateTOTP(secret: string): Promise<TOTPToken | Error> {
    try {
      const token = {
        token: speakeasy.totp({
          secret,
          encoding: "base32",
          window: 1,
          step: 30
        }),
        remaining: 90 - Math.floor((new Date().getTime() / 1000.0) % 90)
      };
      console.log(token);
      return token;
    }
    catch (e) {
      return handleError(e);
    }
  }
}

// ------------------ '(◣ ◢)' ---------------------

function encryptSecret(secret: TOTPSecret, key: string): TOTPSecret | Error {
  const secret_b32 = crypto.encryptAESMessageWithIV(secret.secret_b32, key);
  if (secret_b32 instanceof Error) return secret_b32;
  const secret_crypt: TOTPSecret = {
    genesis: secret.genesis,
    email: secret.email,
    secret_b32,
    exp: secret.exp
  };

  return secret_crypt;
}

function decryptSecret(
  secret_crypt: TOTPSecret,
  key: string
): TOTPSecret | Error {
  const secret_b32 = crypto.decryptAESMessageWithIV(secret_crypt.secret_b32, key);
  if (secret_b32 instanceof Error) return secret_b32;
  const entry: TOTPSecret = {
    genesis: secret_crypt.genesis,
    email: secret_crypt.email,
    secret_b32,
    exp: secret_crypt.exp
  };

  return entry;
}

// ------------------° ̿ ̿'''\̵͇̿̿\з=(◕_◕)=ε/̵͇̿̿/'̿'̿ ̿ °------------------