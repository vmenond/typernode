/*
totp store specifications: api.sats.cc
Matrix Network International B.V.
developed by (•̪●)==ε0O o o oo o o o o O1shi`(•.°)~#
*/

// IMPORTS
import { expect, assert } from "chai";
import "mocha";
import * as  mongo from "../../../../storage/mongo";
import { MongoTOTPStore } from "./mongo";
// ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------
// GLOBAL CONFIGURATIONS
const db = new mongo.MongoDatabase();
const totp_store = new MongoTOTPStore();
const email = "kyc0@test.com";
const secret_b32 = "KVOTUQZFGJXGIQK3KNOVKYSYK4UEIKS6EF3W6ZRYKQUGQJKUNVYA";

// ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------
describe("Initalizing Test: Mongo tfa ", function () {

    before(async function () {
        await db.connect({
            port: process.env.DB_PORT,
            ip: process.env.DB_IP,
            name: 'sats',
            auth: 'auth_server:supersecret',
        }
        );

    });


    describe("create", function () {
        it("SHOULD create a new TOTP Secret Entry ", async function () {
            const response = await totp_store.create({
                genesis: Date.now(),
                email,
                secret_b32,
                exp: Date.now() + 98082
            });
            expect(response).to.equal(true);
        });
    });
    describe("findOne", function () {
        it("SHOULD find an existing TOTP Secret Entry ", async function () {
            const response = await totp_store.findOne(email);
            expect(response['secret_b32']).to.equal(secret_b32);
        });
    });
    describe("updateExp", function () {
        it("SHOULD update the zone expiry time of a TOTP Secret", async function () {
            const response = await totp_store.updateExp(email, Date.now() + 999999);
            expect(response).to.equal(true);
        });
    });
    describe("deleteSecret", function () {
        it("SHOULD delete a TOTP Secret Entry", async function () {
            const response = await totp_store.deleteSecret(email);
            expect(response).to.equal(true);
        });
    });
    describe("updateSecret", function () {
        it("SHOULD update a TOTP Secret Entry", async function () {
            const response = await totp_store.updateSecret(email, secret_b32);
            expect(response).to.equal(true);
        });
    });


});

// ------------------ '(◣ ◢)' ---------------------
