/*
totp store interface: api.sats.cc/auth
#
developed by (•̪●)==ε0O o o oo o o o o O1shi`(•.°)~#
Matrix Network International B.V.
*/

import {TOTPSecret} from "./interface";

export interface TOTPStore{
 create(user: TOTPSecret): Promise<boolean | Error>;
 findOne(email: string): Promise<TOTPSecret | Error>;
 updateExp(email: string,exp: number): Promise<boolean | Error>;
 deleteSecret(email: string):Promise<boolean  |  Error>;
}
