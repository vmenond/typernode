/*
interface: api.sats.cc/auth
#
developed by (•̪●)==ε0O o o oo o o o o O1shi`(•.°)~#
Matrix Network International B.V.
*/

export interface AuthInterface {
 init(): Promise<boolean | Error>;
 authenticate(request: AuthRequest):Promise<AuthResponse | Error>;
};


export enum MFALevel {
    "B1" = "basic",
    "T2" = "totp",
    "P2" = "pin",
    "Z2" = "zone"
   }
   export interface AuthRequest {
    headers: object,
    mfa_level: MFALevel,
   }
   export interface AuthResponse {
    uid?: string;
    email?: string;
    expiry?: number;
    tfa?: boolean
    ip?: string;
    firebase_uid?:string;
   }


export const avatar_list =[
    "wolf",
    "tiger",
    "elephant",
    "bee",
    "ant",
    "eagle",
    "penguin",
    "bear",
    "bull",
    "frog",
    "lion",
    "llama",
    "monkey",
    "dolphin"

]