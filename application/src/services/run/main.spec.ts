/*
api.sats.cc: core specifications
Matrix Network International B.V.
developed by (•̪●)==ε0O o o oo o o o o O1shi`(•.°)~#

# Clear DB
$ mongo

db = db.getSiblingDB('sats')
db.auth('auth_server','supersecret');
let q = {email: 'kyc0@test.com'}
db.profiles.remove(q);
db.sessions.remove(q);
db.tfas.remove(q);
db.bitcoin_accounts.remove(q);

*/

// IMPORTS
import "mocha";
import chai from "chai";
import chaiHttp from "chai-http";
import fs from 'fs';
const sinon = require("sinon");
import { MongoDatabase } from "../../storage/mongo";
import * as  express from "../../network/express";
import { BitcoinCore } from "../core/bitcoin/bitcoind";

import { S5Vault } from "../../lib/kms/vault";
import { S5SpeakEasy } from "../core/auth/totp/speakeasy";
import { S5Auth } from "../core/auth/auth";

const auth = new S5Auth();
const totp = new S5SpeakEasy();
const db = new MongoDatabase();
// ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------
// GLOBAL CONFIGURATIONS
const should = chai.should();
const expect = chai.expect;
chai.use(chaiHttp);
let jwt_token = "eyJhbGciOiJSUzI1NiIsImtpZCI6IjQ5YWQ5YmM1ZThlNDQ3OTNhMjEwOWI1NmUzNjFhMjNiNDE4ODA4NzUiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vc2F0cy1jYyIsImF1ZCI6InNhdHMtY2MiLCJhdXRoX3RpbWUiOjE1OTk4NDQxMjMsInVzZXJfaWQiOiIxWFNRbTJuMHB0UkllNmtrZTdPRWdqaTRQV20xIiwic3ViIjoiMVhTUW0ybjBwdFJJZTZra2U3T0Vnamk0UFdtMSIsImlhdCI6MTU5OTkzODc3MCwiZXhwIjoxNTk5OTQyMzcwLCJlbWFpbCI6Imt5YzBAdGVzdC5jb20iLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsiZW1haWwiOlsia3ljMEB0ZXN0LmNvbSJdfSwic2lnbl9pbl9wcm92aWRlciI6InBhc3N3b3JkIn19.S4E6tO5NRdGVyyEO_yjQnEIW1cx9QOCNgFOd-QA-DKBG9WRwyZ6meJvzk4akKJY002Vdf9mQbC1IxlJ3x9WhgGEBQ1GqmvZoRR0otGZQFCMDyIvipkwHtbCTQMFl3Oj4r3hK2C624Gz2k2624rXrjZLNUIdTA5pO-J8Y7vIkfiW23-4MjQwwHV-NMut0IHnZtBDjOHYARhOWhvNsOmTkbZVzOfIY1gpbGuMgkijFnxsv2wrQZ9TSRuq6D6Pjc-8sOwXkwYIYWYpffAnEhY3XUxvYJGBIbmgWXRludc3_am8qaNQxkvomCzy4WQf389loBMARx_DPyf6O1ip62Ffv0w";
const email = "kyc0@test.com";
const firebase_uid = "AIzaSyBlrfUit357fhPG73BnxJx0VZhEBshIpPQ";
let sid = "test";
let secret = "shhh";
let otp;

let server;


const tx_update0 = {
  txid: `aa3359b41c2ea69c8727e93d553c52ea6a9c427a0bd607df19f8e49efc2f5615`,
  secret: "8s9de5kbu32kx9z4qi32vb"
};
const tx_update_in = {
  txid: `478abb64ce2b062c4806abd065dd3304d10232804e5cbfe6790456f38f48ece2`,
  secret: "8s9de5kbu32kx9z4qi32vb"
}
const tx_update_out = {
  txid: "8d8c114498b132160e255ef46334c516597978170e8a5b44b9c00e680df56235",
  secret: "8s9de5kbu32kx9z4qi32vb"
}

const withdrawal_bit = {
  miner_fees: "low",
  amount_btc: 0.0023,
  notes: "bitred",
  address: "mqGGbaVPDXxpe7MJM5qPios4hJZbCSn1rK",
}

const bad_bit = {
  miner_fees: "low",
  amount_btc: 0.0023,
  notes: "bitred",
  address: "mqGGbaVPDXxpe7MJM5qPios4hJZbCSn1rK1",
}
const platform_balances = {
  market: {
    btc: 12.0231,
    eur: 102309.23
  },
  bank: 12123.34,
  bitcoin: {
    confirmed: 12.2310
  }
}
const receiving_address = "tb1q7km904f6wkpslayem4vtczlvl4k7z8ecxqx6p2";
const admin_pass = "8uij32oei3e";
const admin_otp = "1";
let client_id = "2";
const id_path_true = "/home/ishi/api.sats.cc/typescript/src/misc/richid.jpg";

// ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------
describe("Starting Moltres Behaviour Specification", async function () {
  const sandbox = sinon.createSandbox();
  before(async function () {
    // sandbox.stub(S5Auth.prototype, "authenticate").resolves({ email, uid: firebase_uid });
    sandbox.stub(BitcoinCore.prototype, "generateNewAddress").resolves(receiving_address);
    sandbox.stub(S5Vault.prototype, "getBitcoinRPCPass").resolves('90oi1u238hjk213');

    sandbox.stub();
    await db.connect({
      port: process.env.DB_PORT,
      ip: process.env.DB_IP,

    });
    server = await express.start();
    await auth.init();
  });
  after(function (done) {
    sandbox.restore();
    done();
  });

  /*
  ROUTES 
  */
  /*
 SESSION
  */

  /*
  PROFILE
   */
  describe("GET profile/ - 200", function () {
    it("SHOULD return an initialized profile", function (done) {
      chai
        .request(server)
        .get("/profile/")
        .set("authorization", `Bearer ${jwt_token}`)
        .end((err, res) => {
          res.should.have.status(200);

          console.log(res.body)
          expect(res.body.status).to.equal(true);
          done();
        });
    });
  });

  describe("GET auth/ - 401 (Wrong JWT)", function () {
    it("SHOULD ERROR invalid JWT", function (done) {
      chai
        .request(server)
        .get("/auth")
        .set("authorization", `Bearer 32`)
        .end(async (err, res) => {
          res.should.have.status(401);
          expect(res.body.status).to.equal(false);
          done();
        });
    });
  });

  describe("GET auth/ - 400 (Auth Header typo)", function () {
    it("SHOULD ERROR wrong header name", function (done) {
      chai
        .request(server)
        .get("/auth")
        .set("authorizationz", `Bearer ${jwt_token}`)
        .end(async (err, res) => {
          res.should.have.status(400);
          expect(res.body.status).to.equal(false);
          done();
        });
    });
  });

  describe("GET auth/ - 200", function () {
    it("SHOULD get a totp secret", function (done) {
      chai
        .request(server)
        .get("/auth")
        .set("authorization", `Bearer ${jwt_token}`)
        .end(async (err, res) => {
          res.should.have.status(200);
          expect(res.body.status).to.equal(true);
          console.log(res.body);
          secret = res.body.message.secret;
          otp = await totp.generateTOTP(secret);
          expect(secret.length).to.equal(52);
          done();
        });
    });
  });
  describe("GET auth/ - 409", function () {
    it("SHOULD ERROR secret already shared", function (done) {
      chai
        .request(server)
        .get("/auth")
        .set("authorization", `Bearer ${jwt_token}`)
        .end(async (err, res) => {
          res.should.have.status(409);
          expect(res.body.status).to.equal(false);
          done();
        });
    });
  });
  describe("POST auth/ - 200", function () {
    it("SHOULD successfully authenticate with totp", function (done) {
      chai
        .request(server)
        .post('/auth')
        .set("authorization", `Bearer ${jwt_token}`)
        .set("x-sats-totp", otp.token)
        .end((err, res) => {
          res.should.have.status(200);
          console.log(res.body)
          expect(res.body.status).to.equal(true);
          done();
        });
    });
  });
  describe("POST auth/ - 400", function () {
    it("SHOULD ERROR for invalid totp header name", function (done) {
      chai
        .request(server)
        .post('/auth')
        .set("authorization", `Bearer ${jwt_token}`)
        .set("x-sats-totps", otp.token)
        .end((err, res) => {
          res.should.have.status(400);
          expect(res.body.status).to.equal(false);
          done();
        });
    });
  });
  describe("POST auth/ - 403", function () {
    it("SHOULD ERROR for invalid totp value", function (done) {
      chai
        .request(server)
        .post('/auth')
        .set("authorization", `Bearer ${jwt_token}`)
        .set("x-sats-totp", "999911")
        .end((err, res) => {
          res.should.have.status(403);
          expect(res.body.status).to.equal(false);
          done();
        });
    });
  });

  /*
  WALLET
   */
  otp = totp.generateTOTP(secret);


  describe("POST wallet/bitcoin/genesis - 200", function () {
    it("SHOULD generate a new private bitcoin account", function (done) {
      chai
        .request(server)
        .post("/wallet/bitcoin/genesis")
        .set("authorization", `Bearer ${jwt_token}`)
        .set("x-sats-totp", otp.token)
        .send(postGenBitcoinAccount.body)
        .end((err, res) => {
          res.should.have.status(200);
          expect(res.body.status).to.equal(true);
          // console.log(res.body)
          done();
        });
    });
  });
  describe("POST wallet/bitcoin/genesis - 409", function () {
    it("SHOULD generate a new private bitcoin account", function (done) {
      chai
        .request(server)
        .post("/wallet/bitcoin/genesis")
        .set("authorization", `Bearer ${jwt_token}`)
        .set("x-sats-totp", otp.token)
        .send(postGenBitcoinAccount.body)
        .end((err, res) => {
          res.should.have.status(409);
          expect(res.body.status).to.equal(false);
          // console.log(res.body)
          done();
        });
    });
  });
  describe("POST wallet/bitcoin/notify 0 - 200", function () {
    it("SHOULD update a user bitcoin receive update", function (done) {
      chai
        .request(server)
        .post("/wallet/bitcoin/notify")
        .set("authorization", `Bearer ${jwt_token}`)
        .send(tx_update0)
        .end((err, res) => {
          res.should.have.status(200);
          console.log(err)
          expect(res.body.status).to.equal(true);
          done();
        });
    });
  });
  describe("POST wallet/bitcoin/notify 1 - 200", function () {
    it("SHOULD update a user bitcoin receive update", function (done) {
      chai
        .request(server)
        .post("/wallet/bitcoin/notify")
        .set("authorization", `Bearer ${jwt_token}`)
        .send(tx_update_in)
        .end((err, res) => {
          res.should.have.status(200);
          expect(res.body.status).to.equal(true);
          // console.log(res.body)
          done();
        });
    });
  });
  describe("POST wallet/bitcoin/payment - 202", function () {
    it("SHOULD initiate a psbt for large volume payment", function (done) {
      chai
        .request(server)
        .post("/wallet/bitcoin/payment")
        .set("authorization", `Bearer ${jwt_token}`)
        .set("x-sats-totp", otp.token)
        .send(postWalletTransferBitcoin.body)
        .end((err, res) => {
          res.should.have.status(202);
          expect(res.body.status).to.equal(true);
          console.log(err)
          done();
        });
    });
  });
  describe("POST wallet/bitcoin/payment - 400", function () {
    it("SHOULD fail with bad address error", function (done) {
      chai
        .request(server)
        .post("/wallet/bitcoin/payment")
        .set("authorization", `Bearer ${jwt_token}`)
        .set("x-sats-totp", otp.token)
        .send(postWalletTransferBitcoin400.body)
        .end((err, res) => {
          res.should.have.status(400);
          expect(res.body.status).to.equal(false);
          console.log(err)
          done();
        });
    });
  });
  describe("POST wallet/bitcoin/notify 2 - 200", function () {
    it("SHOULD update a user bitcoin payment update", function (done) {
      chai
        .request(server)
        .post("/wallet/bitcoin/notify")
        .set("authorization", `Bearer ${jwt_token}`)
        .send(tx_update_out)
        .end((err, res) => {
          res.should.have.status(200);
          expect(res.body.status).to.equal(true);
          done();
        });
    });
  });
  describe("GET wallet - 200", function () {
    it("SHOULD get wallet info", function (done) {
      chai
        .request(server)
        .get("/wallet")
        .set("authorization", `Bearer ${jwt_token}`)
        .end((err, res) => {
          res.should.have.status(200);
          console.log(JSON.stringify(res.body, null, 2))
          expect(res.body.status).to.equal(true);
          expect(res.body.message.bitcoin.history).to.have.length(3);
          done();
        });
    });
  });

  /*
SESSION
 */
  describe("GET session/actions - 200", function () {
    it("SHOULD return session actions", function (done) {
      chai
        .request(server)
        .get("/session/actions")
        .query({ skip: 0, limit: 5 })
        .set("authorization", `Bearer ${jwt_token}`)
        .end((err, res) => {
          res.should.have.status(200);
          console.log(res.body)
          expect(res.body.message).to.be.a("array");
          done();
        });
    });
  });


  /*
CLIENT
*/

  describe("GET client/id - 200", function () {
    it("SHOULD create a new client with id for verification", function (done) {
      chai
        .request(server)
        .get("/client/id")
        .set("x-admin-secret", admin_pass)
        .set("x-admin-totp", admin_otp)
        .set("x-client-email", email)
        .end((err, res) => {
          res.should.have.status(200);
          expect(res.body.message).to.be.a("string");
          client_id = res.body.message;
          done();
        });
    });
  });
  describe("POST client/kyc - 200", function () {
    it("SHOULD upload a client ID doc and phone number + forward to admin", function (done) {
      chai
        .request(server)
        .post("/client/kyc")
        .set("x-client-id", client_id)
        .set('content-type', 'application/json')
        .type("form")
        .attach(
          "id",
          fs.readFileSync(id_path_true),
          "richid.jpg"
        )
        .field("phone", "+31909837438")
        .end((err, res) => {
          console.log(JSON.stringify(res.body, null, 2))
          res.should.have.status(200);
          expect(res.body.message).to.equal(true);
          done();
        });
    });
  });
  describe("POST client/verify - 200", function () {
    it("SHOULD verify a client", function (done) {
      chai
        .request(server)
        .post("/client/verify")
        .set("x-admin-secret", admin_pass)
        .set("x-admin-totp", admin_otp)
        .set("x-client-id", client_id)
        .set('content-type', 'application/json')
        .end((err, res) => {
          console.log(JSON.stringify(res.body, null, 2))
          res.should.have.status(200);
          expect(res.body.message).to.equal(true);
          done();
        });
    });
  });

});
// ------------------ '(◣ ◢)' ---------------------
const base_url = 'https://api.sats.cc';
export interface JWTRequestHeaders {
  'authorization': string;
  // `Bearer ${token}`
}
export interface TFARequestHeaders {
  'authorization': string;
  'x-sats-totp': string;
  // 6 digit string
}

const postGenBitcoinAccount = {
  uri: base_url + '/wallet/bitcoin/genesis',
  method: "POST",
  headers: {
    'authorization': `Bearer ${jwt_token}`,
    'x-sats-totp': '565090'
  },
  body: {}
}

const postWalletTransferBitcoin = {
  uri: base_url + `/wallet/bitcoin/payment`,
  method: "POST",
  headers: {
    'authorization': `Bearer ${jwt_token}`,
    'x-sats-totp': '565090'
  },
  body: withdrawal_bit
}
const postWalletTransferBitcoin400 = {
  uri: base_url + `/wallet/bitcoin/payment`,
  method: "POST",
  headers: {
    'authorization': `Bearer ${jwt_token}`,
    'x-sats-totp': '565090'
  },
  body: bad_bit
}


// ------------------ '(◣ ◢)' ---------------------