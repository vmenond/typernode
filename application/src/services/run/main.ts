/*
api.sats.cc: moltres

Fiat-Bitcoin Interface

Matrix Network International B.V.
#
developed by (•̪●)==ε0O o o oo o o o o O1shi`(•.°)~#

-> Firebase JWT Authentication
-> Local Two Factor Authentication
-> Shufti Pro Profile Screening
-> Bunq SEPA Services
-> Bitcoin Core Services
-> Bitvavo Exchange Services
-> Bunq iDEAL Services (coming soon)
-> BigW Debit/Credit Card Processing (coming soon)
-> Veridium ID Biometrics(coming soon)


*/
// ------------------ ┌∩┐(◣_◢)┌∩┐ --------------------
import { logger } from "../../lib/logger/winston";
import { MongoDatabase } from "../../storage/mongo";
import { start as startServer } from "../../network/express";
import { S5Vault } from "../../lib/kms/vault";

const vault = new S5Vault();
const db = new MongoDatabase();
// ------------------ ┌∩┐(◣_◢)┌∩┐ --------------------

vault.getMongoDbAuth().then((mongo_pass) => {
    db.connect({
        port: process.env.DB_PORT,
        ip: process.env.DB_IP,
        name: 'sats',
        auth: mongo_pass as string,
      }).catch(e => {
        logger.error(e);
    })

}).catch((e)=>{
    logger.error(e);
    process.exit(1);
})

startServer().catch(e => {
    logger.error(e);
    process.exit(1);

});

// -----° ̿ ̿'''\̵͇̿̿\з=(◕_◕)=ε/̵͇̿̿/'̿'̿ ̿ °-----------
