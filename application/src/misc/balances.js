db = db.getSiblingDB('sats')
db.auth('auth_server','supersecret');

let uemail="sats.cloud@gmail.com";
/* user history from btxs */
db.btxs.find({
    "$or": [
        {to: uemail},
        {from: uemail}
    ]
}).pretty();

/* user balance from btxs */
db.btxs.aggregate([
    { $group: {
        _id: {
            to:uemail,
            from:uemail
        },
        balance: {$sum: "$amount"}   
    }}
]).pretty();

/* total balance from btxs */
db.btxs.aggregate([
    {$group: {
        _id: null,
        total_balance: {$sum: "$amount"},
    }}
]).pretty();


