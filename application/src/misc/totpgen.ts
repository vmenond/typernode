import speakeasy = require("speakeasy");

export function get_tfa_token(secret: string) {
  return {
    token: speakeasy.totp({
      secret: secret,
      encoding: "base32",
      window: 2,
      step: 30
    }),
    remaining: 90 - Math.floor((new Date().getTime() / 1000.0) % 90)
  };
}
