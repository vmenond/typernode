/*
api.sats.cc :: lib : handle_response
Matrix Network International B.V.
#
developed by (•̪●)==ε0O o o oo o o o o O1shi`(•.°)~#
*/
// ------------------ '(◣ ◢)' ---------------------
import * as fs from "fs";
import * as crypto from "crypto";
import{S5UID}from "../lib/uid/uid";
import { logger, r_500 } from "../lib/logger/winston";
import { handleError } from '../lib/errors/e';
import { MFALevel } from "../services/core/auth/interface";

// ------------------ '(◣ ◢)' ---------------------
const KEY_PATH = `${process.env.HOME}/.keys`;
const KEY_NAME = "sats_sig";

const s5uid = new S5UID();
// ------------------ '(◣ ◢)' ---------------------
// try sending a request without header
export function parseRequest(request: any): S5Request {
  const r_custom: S5Request = {
    method: request.method || "method_error",
    resource: request.originalUrl || "resource_error",
    headers: request.headers || "headers_error",
    body: request.body || "body_error",
    user: request.user || "private",
    files: request.files || "zil",
    file: request.file || "zil",
    timestamp: Date.now(),
    gmt: new Date(Date.now()).toUTCString(),
    ip: request.headers["x-forwarded-for"] || "ip_error",
    params: request.params || {},
    device: request.headers['user-agent'] || "unknown",
    query: request.query
  };
  return r_custom;
}
// ------------------ '(◣ ◢)' ---------------------
export async function respond(
  status_code: number,
  message: any,
  response: any,
  request: any
) {
  try {
    const sats_id = s5uid.createResponseID();
    const now = Date.now();
    const headers = {
      "x-s5-id": sats_id,
      "x-s5-time": now
    };

    const signature = await getResponseSignature(
      status_code,
      request["resource"],
      request["method"],
      headers,
      message
    );
    if (signature instanceof Error) {
      return signature;
    }
    const headers_with_sig: S5ResponseHeaders = {
      "x-s5-id": sats_id,
      "x-s5-time": now,
      "x-s5-signature": signature
    };
    logger.info({ user: ((request.user.uid)?request.user.uid:"external"), resource: `${request['method']}-${(request["resource"] || request['originalUrl'])}`, status_code })
    return (
      response
        .set(headers_with_sig)
        .status(status_code)
        .send(message)
    );
  } catch (e) {
    logger.error("Ourskirts error at dto respond", e);
    let message = r_500;
    switch (e.code) {
      case 401:
        message = {
          status: false,
          message: e.message
        };
        break;

      case 400:
        message = {
          status: false,
          message: e.message
        };
        break;

      case 500:
        message = {
          status: false,
          message: e.message
        };
        break;

      default:
        message = {
          status: false,
          message: "Internal Signing Error"
        };
        e.code = 500;
        break;
    }
    return (response.status(e.code).send(message));
  }
}
// ------------------ '(◣ ◢)' ---------------------
export async function getResponseSignature(
  status_code: number,
  ep: string,
  method: string,
  headers: S5ResponseHeaders,
  body: S5Output
): Promise<string | Error> {
  try {

    if (fs.existsSync(`${KEY_PATH}/${KEY_NAME}.pem`)) {
      const private_key = fs
        .readFileSync(`${KEY_PATH}/${KEY_NAME}.pem`)
        .toString("ascii");


      
      const message = `${status_code}-${headers["x-s5-id"]}-${headers["x-s5-time"]}`;

      // RESPONSE WITHOUT BODY

      const sign = crypto.createSign("RSA-SHA256");
      sign.update(message);
      sign.end();

      const signature = sign.sign({key: private_key,passphrase: "test"}, 'base64');

      const status = await checkResponseSignature(status_code,headers,signature);
      if (status instanceof Error) return status;
      return signature;
    }
    else {
      logger.error("No response signing key found!. Run $ ditto crpf sats_sig")
      return handleError({
        code: 500,
        message: "No response signing key found!"
      })
    }

  } catch (e) {
    logger.error(e);
    return handleError(e);
  }
}
// ------------------ '(◣_◢)' ------------------
export async function checkResponseSignature(
  status_code: number,
  headers: S5ResponseHeaders,
  sig_b64: string // base64
): Promise<boolean | Error> {
  try {
    const signature = Buffer.from(sig_b64, 'base64')

    const public_key = fs
      .readFileSync(`${KEY_PATH}/${KEY_NAME}.pub`)
      .toString("ascii");

    const message = `${status_code}-${headers["x-s5-id"]}-${headers["x-s5-time"]}`;

    const verify = crypto.createVerify("RSA-SHA256");
    verify.update(message);
    // verify.end();

    return (verify.verify(Buffer.from(public_key, 'ascii'), signature));
  }
  catch (e) {
    return handleError(e);
  }
}
// ------------------ '(◣ ◢)' ---------------------
export function filterError(
  e,
  custom_500_message: object,
  request_data: S5Request
): S5Body {
  let code: number = 500;
  let message = custom_500_message;
  const s_codes = ["202", "400", "401", "402", "403", "404","406", "409", "415", "420", "422"];
  const n_codes = [202, 400, 401, 402, 403, 404,406, 409, 415, 420, 422];

  if (e instanceof Error && s_codes.includes(e.name)) {
    code = parseInt(e.name, 10);
  }
  // just to not break old error format

  else if (e.code && typeof (e.code) == 'number') {
    code = e["code"];
  }

  // logger.warn({
  //   code,
  //   resource:request_data.resource,
  //   method: request_data.method,
  //   e: e['message'],
  //   user: (request_data.user) ? request_data.user.email : request_data.ip
  // });

  if(code === 400) logger.debug({e})

  // important that these codes are numbers and not strings
  // node.js erorrs return strings, custom is number, ogay?
  if (n_codes.includes(code)) {
    // Client Errors: Change message from default 500
    try {
      message = {
        status: false,
        message: JSON.parse(e["message"])
      };
    }
    catch (err) {
      message = {
        status: false,
        message: e["message"]
      };
    }

  } else {
    // Server Errors: Leave message as default 500
    // request_data["headers"] = undefined;

    logger.error({
      request: {
        body: request_data['body'],
        resource: request_data['resource'],
        ip: request_data.ip || "no ip",
      },
      e
    });
  }
  return {
    code,
    message
  };
}
// ------------------ '(◣ ◢)' ---------------------
export interface S5RequestHeaders {
  'authorization'?: string;
  'x-s5-totp'?: string;
}
export interface S5ResponseHeaders {
  'x-s5-id': string;
  'x-s5-time': number;
  'x-s5-signature'?: string;
}

export interface S5User {
  uid?: string;
  email?: string;
  expiry?: number;
  tfa?: boolean;
  auth_level?: MFALevel;
  ip?: string;
}
export interface S5Input {
  email?: string;
  time?: number;
  quote?: number;
  type?: string ;
  nickname?: string;
  text?: string;
  full_address: string;
  full_name: string;
  add?: string;
  add_type?: string;
  id?: string;
  id_doc?:string;
  id_type?: string;
  phone?: string;
  address?: string;
  iban?: string;
  bic?: string;
  number?: string;
  exp?: string;
  issue?: string;
  amount_btc?: number;
  amount_fiat?: number;
  order_id?: string;
  notices?: Array<object>;
  status?: string;
  txid?: string;
  pc_code?: string;
  libra_beneficiary?: string;
  s5amount?: number;
  amount?: number;
  fees?: string ;
  n_acts?: number;
  tier?: number;
  referred_by?: string;
  secret?: string;
  uid?: string;
  section?: string;
  skip?: string;
  limit?: string;
  hex?: string;
  notes?: string;
  avhash?:string;

  };

export interface S5Output {
  sid: string;
}
export interface S5Request {
  headers?: object;
  body?: S5Input;
  method?: string;
  resource?: string;
  ip?: string | string[];
  timestamp?: number;
  gmt?: string;
  user?: S5User;
  files?: string | object;
  file?: string | object;
  params?: S5Input;
  device?: string;
  query?: S5Input;
}
export interface S5Body {
  code: number;
  message: object | string;
}
export interface S5Response {
  headers: S5ResponseHeaders;
  body: S5Body
}

// ------------------ '(◣ ◢)' ---------------------