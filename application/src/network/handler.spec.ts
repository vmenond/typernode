/*
api.sats.cc: dto handler specifications
Matrix Network International B.V.
developed by (•̪●)==ε0O o o oo o o o o O1shi`(•.°)~#
*/

import { expect } from "chai";
import "mocha";
import * as sinon from "sinon";
import * as handler from "./handler";
import {S5UID} from "../lib/uid/uid";

const s5uid = new S5UID();
// ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------
// GLOBAL CONFIGURATIONS
const status_code = 200;
const ep = "/session/login";
const method = "POST";
const sats_id = s5uid.createResponseID();
const sid = s5uid.createSessionID(); 
const now = Date.now();
const headers = {
  "x-s5-id": sats_id,
  "x-s5-time": now
};
const out = {
 sid
}
let sig;
// ------------------ ┌∩┐(◣_◢)┌∩┐ ------------------
describe("Initalizing Test: DTO Handler ", function () {

 describe("getResponseSignature", function () {
  it("SHOULD return a signed response", async function () {
   const response = await handler.getResponseSignature(status_code,ep,method,headers,out);
   sig = response;
   expect(response).to.be.a('string');
  });
 });
 describe("checkResponseSignature", function () {
  it("SHOULD check a signed response", async function () {
   const response = await handler.checkResponseSignature(status_code,headers,sig)
   console.log({response})
   expect(response).to.equal(true);
  });
 });

});

// ------------------ '(◣ ◢)' ---------------------
