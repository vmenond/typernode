/*
api.sats.cc: auth_server controllers: init_server
Matrix Network International B.V.
#
developed by (•̪●)==ε0O o o oo o o o o O1shi`(•.°)~
*/
// ------------------ '(◣ ◢)' ---------------------

import express from "express";
import helmet from "helmet";
import bodyParser from "body-parser";
import * as path from "path";
import * as fs from "fs";

import { logger } from "../lib/logger/winston";
import { respond } from "./handler";

import { router as  auth } from "../services/core/auth/router";
import { router as  bitcoin } from "../services/core/bitcoin/router";


const base_path = `${process.env.HOME}/src/services/core/admin/`

// ------------------ '(◣ ◢)' ---------------------
export async function start() {
  return new Promise(async (resolve, reject) => {
    try {
      const PORT = process.env.MOLTRES_PORT;
      console.log({PORT});
      const server = express();
      server.use(helmet());
      server.use(bodyParser.json());
      server.use(
        bodyParser.urlencoded({
          extended: true
        })
      );

      server.set("etag", false);
      server.disable("x-powered-by");

      server.use("/bitcoin",bitcoin);     
      server.use("/admin", auth);
      
      
      const options = {
        dotfiles: 'ignore',
        etag: false,
        extensions: ['htm','html','css','js','pdf','svg','jpeg','jpg','png'],
        index: false,
        maxAge: '1d',
        redirect: false,
        setHeaders: function (res, path, stat) {
          res.set('X-Content-Type-Options', 'no-sniff');
        }
      }
      server.use(express.static(path.join(base_path,"public"),options));

      server.get("/", (request, response) => {
        (fs.existsSync(path.join((base_path+"/public/"), "home.html")))?
        response.sendFile(path.join((base_path+"/public/"), "home.html")):
        response.sendStatus(501);
      });

      const app = server.listen(PORT, async() => {
        console.log(true,Date.now())
        resolve(app)
      });

      // Gracefully terminate server on SIGINT AND SIGTERM
      process.on("SIGINT", () => {
        logger.info({
          SIGINT: "Got SIGINT. Gracefully shutting down Http server"
        });
        app.close(() => {
          logger.info("Http server closed.");
        });
      });

      // quit properly on docker stop
      process.on("SIGTERM", () => {
        logger.info({
          SIGTERM: "Got SIGTERM. Gracefully shutting down Http server."
        });

        app.close(() => {
          logger.info("Http server closed.");
        });
      });

      const sockets = {};

      let nextSocketId = 0;

      app.on("connection", socket => {
        const socketId = nextSocketId++;
        sockets[socketId] = socket;

        socket.once("close", function () {
          delete sockets[socketId];
        });
      });
    } catch (e) {
      reject(e);
    }
  });
};

// ------------------ '(◣ ◢)' ---------------------